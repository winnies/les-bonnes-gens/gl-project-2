# Look for a version of Tiled on the local machine
#
# By default, this will look in all common places. If Tiled is built or
# installed in a custom location, you're able to either modify the
# CMakeCache.txt file yourself or simply pass the path to CMake using either the
# environment variable `TILED_ROOT` or the CMake define with the same name.

set(TILED_PATHS
    ${TILED_ROOT}
	$ENV{TILED_ROOT}
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw
	/opt/local
	/opt/csw
	/opt
)

find_path(TILED_INCLUDE_DIR tiled/tiled.h PATH_SUFFIXES include PATHS ${TILED_PATHS})
find_library(TILED_LIBRARY NAMES tiled PATH_SUFFIXES lib PATHS ${TILED_PATHS})

set(TILED_LIBRARIES ${TILED_LIBRARY})
set(TILED_INCLUDE_DIRS ${TILED_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set TILED_FOUND to TRUE if
# all listed variables are TRUE
find_package_handle_standard_args(TILED DEFAULT_MSG TILED_LIBRARY TILED_INCLUDE_DIR)

mark_as_advanced(TILED_INCLUDE_DIR TILED_LIBRARY)
