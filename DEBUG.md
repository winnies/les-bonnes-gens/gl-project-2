# Env variables

| Nom            | Description                                 |
|----------------|---------------------------------------------|
| RESOURCES_PATH | Change le dossier où charger les ressources |
| LEVEL          | Change le niveau chargé par défaut          |
