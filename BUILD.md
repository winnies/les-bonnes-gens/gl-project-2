# Dependencies

Qt 5.9<
EntityX 1.3
Tiled v1.3.0
extra-cmake-modules v5.63.0

# Build

```
clone https://gitlab.com/winnies/gl-project-2.git
cd gl-project-2
mkdir build && cd build
cmake ..
make
```

# Run

```
./build/gl-project-2
```

# Build at university

```
cd gl-project-2
source ./scripts/setup_at_university.sh
```

# Build docker image and publish

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/winnies/gl-project-2 .
docker push registry.gitlab.com/winnies/gl-project-2
```

# Run ci

```
gitlab-runner exec docker build
```

With local image:

```
gitlab-runner exec docker --docker-pull-policy=never build
```
