# use clang-tidy
targets:
 * clang-tidy
 * clang-tidy-fix

# use run-clang-tidy.py (no colors, duplicate outputs, quicker)
targets:
 * clang-tidy-parallel
 * clang-tidy-fix-parallel
