<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="basic" tilewidth="16" tileheight="16" tilecount="57" columns="19">
 <image source="basic.png" width="304" height="48"/>
 <terraintypes>
  <terrain name="Mur épais" tile="1"/>
  <terrain name="Mur fin" tile="4"/>
  <terrain name="Mur carré" tile="7"/>
  <terrain name="Sol" tile="50"/>
 </terraintypes>
 <tile id="0" type="Wall" terrain="3,3,3,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="1" type="Wall" terrain="3,3,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="2" type="Wall" terrain="3,3,0,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="3" type="Wall" terrain="3,3,3,1">
  <objectgroup draworder="index">
   <object id="1" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="4" type="Wall" terrain="3,3,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="5" type="Wall" terrain="3,3,1,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="6" type="Wall" terrain="3,3,3,2">
  <objectgroup draworder="index">
   <object id="1" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="7" type="Wall" terrain="3,3,2,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="8" type="Wall" terrain="3,3,2,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="9" type="Wall" terrain="0,0,3,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="10" type="Wall" terrain="0,0,1,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="11" type="Wall" terrain="0,3,0,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="12" type="Wall" terrain="3,0,1,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
   <object id="2" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="13" type="Wall" terrain="0,0,0,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
   <object id="2" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="14" type="Wall" terrain="0,0,3,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="15" type="Wall" terrain="1,1,1,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="16" type="Wall" terrain="1,1,3,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="19" type="Wall" terrain="3,0,3,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="20" type="Wall" terrain="0,0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="21" type="Wall" terrain="0,3,0,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="22" type="Wall" terrain="3,1,3,1">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="23" type="Wall" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="24" type="Wall" terrain="1,3,1,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="25" type="Wall" terrain="3,2,3,2">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="26" type="Wall" terrain="2,2,2,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="27" type="Wall" terrain="2,3,2,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="28" type="Wall" terrain="3,1,0,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
   <object id="2" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="29" type="Wall" terrain="1,3,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="30" type="Wall" terrain="0,1,0,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="31" type="Wall" terrain="1,0,3,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="32" type="Wall" terrain="0,3,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="33" type="Wall" terrain="3,0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
   <object id="2" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="34" type="Wall" terrain="1,3,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
   <object id="2" x="8" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="35" type="Wall" terrain="3,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
   <object id="2" x="0" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="38" type="Wall" terrain="3,0,3,3">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="39" type="Wall" terrain="0,0,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="40" type="Wall" terrain="0,3,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="41" type="Wall" terrain="3,1,3,3">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="42" type="Wall" terrain="1,1,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="43" type="Wall" terrain="1,3,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="44" type="Wall" terrain="3,2,3,3">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="45" type="Wall" terrain="2,2,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="46" type="Wall" terrain="2,3,3,3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="47">
  <objectgroup draworder="index">
   <object id="1" x="0" y="10" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="48" type="Coin">
  <objectgroup draworder="index">
   <object id="1" x="6" y="6" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="49" type="Fruit">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="50" type="Floor" terrain="3,3,3,3"/>
</tileset>
