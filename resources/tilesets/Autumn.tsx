<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="" tilewidth="32" tileheight="32" tilecount="66" columns="22">
 <image source="../sprites/Autumn/Autumn.png" width="716" height="96"/>
 <terraintypes>
  <terrain name="Mur" tile="19"/>
 </terraintypes>
 <tile id="0" type="Wall" terrain=",,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1" type="Wall" terrain=",,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="2" type="Wall" terrain=",,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="12" type="Wall" terrain="0,0,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="32"/>
   <object id="2" x="16" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="13" type="Wall" terrain="0,0,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="16"/>
   <object id="2" x="16" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="14" type="PasseMuraille">
  <objectgroup draworder="index" id="3">
   <object id="3" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="15" type="MapleSyrup">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="16" type="Floor" probability="0.15"/>
 <tile id="17" type="Floor" probability="0.15"/>
 <tile id="18" type="Floor"/>
 <tile id="19" type="Floor" probability="0.4"/>
 <tile id="20" type="Floor" probability="0.4"/>
 <tile id="21" type="Bedrock">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="22" type="Wall" terrain=",0,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0" width="16" height="32"/>
  </objectgroup>
 </tile>
 <tile id="23" type="Wall" terrain="0,0,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="24" type="Wall" terrain="0,,0,">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0" y="0" width="16" height="32"/>
  </objectgroup>
 </tile>
 <tile id="34" type="Wall" terrain="0,,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="32"/>
   <object id="2" x="16" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="35" type="Wall" terrain=",0,0,0">
  <objectgroup draworder="index" id="3">
   <object id="2" x="16" y="0" width="16" height="16"/>
   <object id="3" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="36" type="Door">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="37" type="GhostPath"/>
 <tile id="38" type="Floor" terrain="0,,," probability="0.15"/>
 <tile id="39" type="Floor" probability="0.15"/>
 <tile id="40" type="Floor" probability="0.15"/>
 <tile id="41" type="Floor" probability="0.15"/>
 <tile id="42" type="Floor" probability="0.4"/>
 <tile id="44" type="Wall" terrain=",0,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="45" type="Wall" terrain="0,0,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="46" type="Wall" terrain="0,,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="56" type="Floor"/>
 <tile id="57" type="Fruit">
  <objectgroup draworder="index" id="3">
   <object id="5" x="1" y="1" width="30" height="29"/>
  </objectgroup>
 </tile>
 <tile id="58" type="Coin">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9" y="7" width="14" height="17"/>
  </objectgroup>
 </tile>
 <tile id="60" type="Floor" probability="0.5"/>
 <tile id="61" type="Floor" probability="0.5"/>
 <tile id="62" type="Floor" probability="0.15"/>
 <tile id="63" type="Floor" probability="0.15"/>
 <tile id="64" type="Floor" probability="0.4"/>
</tileset>
