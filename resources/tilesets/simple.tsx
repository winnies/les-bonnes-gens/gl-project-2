<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="Blue walls" tilewidth="16" tileheight="16" tilecount="18" columns="6">
 <image source="simple.png" width="96" height="48"/>
 <terraintypes>
  <terrain name="Mur" tile="10"/>
  <terrain name="Sol" tile="2"/>
 </terraintypes>
 <tile id="0" type="Wall" terrain="0,0,0,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="7" height="16"/>
   <object id="2" x="7" y="0" width="9" height="7"/>
  </objectgroup>
 </tile>
 <tile id="1" type="Wall" terrain="0,0,1,0"/>
 <tile id="2" type="Floor" terrain="1,1,1,1"/>
 <tile id="3" type="Wall" terrain="1,1,1,0"/>
 <tile id="4" type="Wall" terrain="1,1,0,0"/>
 <tile id="5" type="Wall" terrain="1,1,0,1"/>
 <tile id="6" type="Wall" terrain="0,1,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="7" height="16"/>
   <object id="2" x="7" y="9" width="9" height="7"/>
  </objectgroup>
 </tile>
 <tile id="7" type="Wall" terrain="1,0,0,0"/>
 <tile id="9" type="Wall" terrain="1,0,1,0"/>
 <tile id="10" type="Wall" terrain="0,0,0,0"/>
 <tile id="11" type="Wall" terrain="0,1,0,1"/>
 <tile id="15" type="Wall" terrain="1,0,1,1"/>
 <tile id="16" type="Wall" terrain="0,0,1,1"/>
 <tile id="17" type="Wall" terrain="0,1,1,1"/>
</tileset>
