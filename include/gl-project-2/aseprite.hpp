#ifndef GL_PROJECT_2_ASEPRITE_HPP
#define GL_PROJECT_2_ASEPRITE_HPP

#include "gl-project-2/ecs/components.hpp"

QVector<Sprite::Frame> readAsepriteFrames(const QString& fileName);

#endif // GL_PROJECT_2_ASEPRITE_HPP
