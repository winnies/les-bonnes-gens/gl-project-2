#ifndef GL_PROJECT_2_ASTAR_ASTAR_HPP
#define GL_PROJECT_2_ASTAR_ASTAR_HPP

#include "gl-project-2/astar/grid.hpp"
#include <QPoint>
#include <QVector>

class Astar {
  public:
    Grid grid;

    Astar(const QVector<bool>& boules, int columnNumber);
    Astar(const Grid& grid);
    QVector<QPoint> findPath(const QPoint& startPos, const QPoint& targetPos);

  private:
    QVector<QPoint> retracePath(const Node& startNode, const Node& endNode);
};

#endif // GL_PROJECT_2_ASTAR_ASTAR_HPP
