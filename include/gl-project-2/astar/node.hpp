#ifndef GL_PROJECT_2_ASTAR_NODE_HPP
#define GL_PROJECT_2_ASTAR_NODE_HPP

#include <QPoint>

class Node {
  public:
    bool walkable;
    int gCost; // how far away the node is from the start Node
    int hCost; // how far away the node is from the end Node

    QPoint coord;
    QPoint parentCoord;

    Node();
    Node(const QPoint& coord, bool walkable);

    inline int fCost() const {
        // std::cout << "[Node at " << gridX << "," << gridY << "] -> hcost = " << hCost << " gcost = " << gCost << std::endl;
        return hCost + gCost;
    }

    inline bool operator==(const Node& a) const { return coord == a.coord; }
};

#endif // GL_PROJECT_2_ASTAR_NODE_HPP
