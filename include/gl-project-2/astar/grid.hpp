#ifndef GL_PROJECT_2_ASTAR_GRID_HPP
#define GL_PROJECT_2_ASTAR_GRID_HPP

#include "gl-project-2/astar/node.hpp"
#include <QPoint>
#include <QVector>

using namespace std;

class Grid {
  public:
    int columnNumber;
    int lignNumber;
    QVector<Node> grid;

    Grid();
    Grid(const QVector<bool>& boules, int columnNumber);

    inline Node& get(const QPoint& coord) { return grid[coord.x() * columnNumber + coord.y()]; }

    QVector<Node> getNeighbours(const Node& node);
    void displayGrid();
};

#endif // GL_PROJECT_2_ASTAR_GRID_HPP
