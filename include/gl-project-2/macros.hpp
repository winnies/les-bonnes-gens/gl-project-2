#ifndef GL_PROJECT_2_MACROS_HPP
#define GL_PROJECT_2_MACROS_HPP
// clang-format off

#define DISABLE_COPY(Class) Class(const Class&) = delete; Class& operator=(const Class&) = delete; // NOLINT
#define DISABLE_MOVE(Class) Class(Class&&) = delete; Class& operator=(Class&&) = delete; // NOLINT
#define DISABLE_COPY_MOVE(Class) DISABLE_COPY(Class) DISABLE_MOVE(Class) // NOLINT

#endif // GL_PROJECT_2_MACROS_HPP
