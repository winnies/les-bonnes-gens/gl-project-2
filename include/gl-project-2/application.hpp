#ifndef GL_PROJECT_2_APPLICATION_HPP
#define GL_PROJECT_2_APPLICATION_HPP

#include "gl-project-2/macros.hpp"
#include <QtWidgets>

class Application {
    DISABLE_COPY_MOVE(Application);

  private:
    QApplication app; // must be the last to be deallocated

  public:
    explicit Application(int& argc, char* argv[]); // NOLINT
    ~Application();

    static int exec();

  private:
    QWidget* window;
};

#endif // GL_PROJECT_2_APPLICATION_HPP
