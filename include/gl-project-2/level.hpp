#ifndef GL_PROJECT_2_LEVEL_HPP
#define GL_PROJECT_2_LEVEL_HPP

#include "gl-project-2/ecs/events.hpp"
#include "gl-project-2/macros.hpp"
#include <QtWidgets>
#include <entityx/entityx.h>
#include <tiled/map.h>
#include <tiled/mapreader.h>

class Level
: public QWidget
, public entityx::Receiver<Level> {
    Q_OBJECT
    DISABLE_COPY_MOVE(Level);
    QLabel* score;
    QLabel* highScore;
    long bestScore;
    QLabel* life;

  public:
    explicit Level(QWidget* parent = nullptr);
    ~Level() override;

    void receive(const GameplayEvent& gameplayEvent);

  private:
    void update();
    void loadMap(const QString& fileName);
    void loadMap(const Tiled::Map& map);

  private:
    const int updateTimeMsec = 16;
    QTimer* timer;
    QTime elapsedTime;

    entityx::EventManager events;
    entityx::EntityManager entities;
    entityx::SystemManager systems;

    Tiled::MapReader mapReader;
    std::unique_ptr<Tiled::Map> map;

    bool initGameplay = true;
};

#endif // GL_PROJECT_2_LEVEL_HPP
