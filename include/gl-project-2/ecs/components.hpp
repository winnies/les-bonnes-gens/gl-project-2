#ifndef GL_PROJECT_2_ECS_COMPONENTS_HPP
#define GL_PROJECT_2_ECS_COMPONENTS_HPP

#include "gl-project-2/astar/grid.hpp"
#include <QGraphicsPixmapItem>
#include <QPoint>
#include <QPointF>
#include <QRectF>
#include <QSet>
#include <QSharedPointer>
#include <QString>
#include <QVector>
#include <entityx/entityx.h>

struct Exists {
    bool exists;

    Exists(bool exists = true) : exists(exists) {}
};

inline bool exists(entityx::Entity entity) {
    auto exists = entity.component<Exists>();
    return !exists || (exists && exists->exists);
}

struct DestroyLater {};

struct Debug {};

using Position = QPointF;
using Velocity = double;

/*!
 * \struct Direction
 * \brief direction of the entity.
 */
struct Direction {
    /**
     * \brief Physics addition (to access actualDirection).
     * \relates Physics
     */
    friend class Physics;

  public:
    enum class TypeDirection { Up, Down, Left, Right }; /*!< Type of direction.*/
    TypeDirection futurDirection;                       /*!< The direction we want to replace in the actualDirection variable.*/

    /*!
     *  \fn Constructor
     *
     *  \brief Constructor of the Direction structure.
     *
     *  \param direction : Type of direction for the entity
     */
    Direction(const TypeDirection& direction) : futurDirection(direction), actualDirection(direction) {}

    /*!
     * \fn different
     * \brief To find out if actualDirection and futureDirection are  different.
     *
     * \return Returns a boolean, TRUE if there is same type direction | FALSE if there is no same type direction.
     *
     */
    inline bool different() { return (actualDirection != futurDirection); }

    /*!
     * \fn differentAxe
     * \brief To find out if actualDirection and futureDirection are of different axis.
     *
     * \return Returns a boolean, TRUE if there is same axes | FALSE if there is no same axes.
     *
     */
    bool differentAxe();

    /*!
     * \fn getActualDirection
     * \brief return private variable (actualDirection)
     *
     * \return Returns TypeDirection
     *
     */
    inline TypeDirection getActualDirection() const { return actualDirection; }

  private:
    TypeDirection actualDirection; /*!< The direction actual of entity.*/
};

inline uint qHash(const Direction::TypeDirection& key, uint seed) {
    return qHash(static_cast<int>(key), seed);
}

enum class Role { PacMan, Ghost, Coin, Fruit, PasseMuraille, MapleSyrup };

struct PasseMuraille {
    double remainingMsec;

    PasseMuraille(double remainingMsec) : remainingMsec(remainingMsec) {}
};

struct MapleSyrup {
    double remainingMsec;

    MapleSyrup(double remainingMsec) : remainingMsec(remainingMsec) {}
};

struct Spawn {
    QPointF position;

    Spawn(const QPointF& position) : position(position) {}
};

struct InputMode {
    Qt::Key leftKey;
    Qt::Key upKey;
    Qt::Key rightKey;
    Qt::Key downKey;

    InputMode(Qt::Key leftKey, Qt::Key upKey, Qt::Key rightKey, Qt::Key downKey)
    : leftKey(leftKey), upKey(upKey), rightKey(rightKey), downKey(downKey) {}
};

struct Target {
    enum class Kind { Entity, Position };

    Target(const entityx::Entity& entity);
    Target(const QPointF& position);

    Kind kind() const { return targetKind; }
    entityx::Entity entity() const;
    QPointF position() const;

  private:
    Kind targetKind;
    union {
        entityx::Entity dataEntity;
        QPointF dataPosition;
    };
};

struct RandomAI {
    double elapsedTime = 0;
    double decisionTime;

    RandomAI(double decisionTime = 200) : decisionTime(decisionTime) {}
};

struct RangeAI {
    int range = 0;

    RangeAI(int range = 8 * 32) : range(range) {}
};

struct SmartAI {
    Grid grid;
};

struct ChameleonAI {
    double shiftTime;
    double elapsedTime = 0;

    ChameleonAI(double shiftTime = 9000) : shiftTime(shiftTime), elapsedTime(shiftTime) {}
};

struct RawSpeed {
    double speed;

    RawSpeed(double speed) : speed(speed) {}
};

struct GhostPath {};

/*!
 * \struct CollisionBoxes
 * \brief Hitboxes.
 *
 * This is intended to identify collisions between entities.
 */
struct CollisionBoxes {
    QSet<QString> mask;    /*! < List of entities that have collision with my entity.*/
    QVector<QRectF> boxes; /*! < List of collision boxes owned by the entity.*/
    QString layer;         /*! < Type of entity (player / wall / .. ?).*/
    QSizeF globalSize;     /*! < Size total of the entity.*/

    /*!
     *  \fn Constructor
     *
     *  \brief Constructor of the Collisionboxes structure.
     *
     *  \param _boxes : List of collision boxes owned by the entity.
     *  \param layer : Type of entity.
     */
    CollisionBoxes(QVector<QRectF> boxes, QString layer);

    /*!
     *  \fn Constructor
     *
     *  \brief Constructor of the Collisionboxes structure.
     *
     *  \param box : A collision box defined for the entity.
     *  \param layer : Type of entity.
     */
    CollisionBoxes(QRectF box, QString layer);

    /*!
     * \fn addLayer
     * \brief Add layer to set Mask.
     *
     */
    inline void addLayer(QString newLayer) { mask.insert(std::move(newLayer)); }

    /*!
     * \fn addMask
     * \brief Add array of layer to set Mask.
     *
     */
    void addMask(const QVector<QString>& newLayers);

    /*!
     * \fn replaceMask
     * \brief Change set Mask by an other array.
     *
     */
    void replaceMask(const QVector<QString>& newLayers);

    /*!
     * \fn removeLayer
     * \brief Remove a layer to a set Mask
     *
     */
    inline void removeLayer(QString testLayer) { mask.remove(testLayer); }

    /*!
     * \fn getLayer
     *
     * \return Returns layer of the entity.
     *
     */
    inline QString getLayer() const { return layer; }

    /*!
     * \fn plusScaleBox
     * \brief Scale Width and Height by incrementing by a number.
     *
     * \param size : size we want change.
     * \param plus : nomber to scale the size.
     * \return Returns new size with the transformations.
     *
     */
    static QSizeF plusScaleBox(const QSizeF& size, const double& plus);

  private:
    /*!
     * \fn totalSize
     *
     * \return Returns the global size of the entity
     *
     */
    QSizeF totalSize();
};

struct Sprite {
    friend class Render;

    struct Frame {
        double durationMsec;
        QPixmap pixmap;
    };

    using Frames = QVector<Frame>;

    Frames frames;
    int currentFrame;
    QSize size;
    double elapsedTimeMsec = 0.0;
    QSharedPointer<QGraphicsPixmapItem> pixmapItem;

  private:
    Sprite(const Frames& frames, int currentFrame, QSize size);
};

struct DirectionToFramesMap {
    QHash<Direction::TypeDirection, Sprite::Frames> frames;
};

struct GhostSoul {
    enum class Color { Red, Green, Blue, Yellow };

    enum class State { Waiting, ComingOut, Hungry, Fearful, Loser };

    Color color;
    State state;
    double waitingMsec;
    DirectionToFramesMap normalFrames;
    DirectionToFramesMap fearfulFrames;
    DirectionToFramesMap loserFrames;
    Position outside;
    double remainingMsec;
    entityx::Entity pacman;

    GhostSoul(Color color,
              State state,
              double waitingMsec,
              const DirectionToFramesMap& normalFrames,
              const DirectionToFramesMap& fearfulFrames,
              const DirectionToFramesMap& loserFrames);
};

#endif
