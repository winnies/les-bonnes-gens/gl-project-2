#ifndef GL_PROJECT_2_ECS_EVENTS_HPP
#define GL_PROJECT_2_ECS_EVENTS_HPP

#include <entityx/entityx.h>

class GameplayEvent {
  public:
    enum class Kind {
        GameStarted,
        GameEnded,
        GameWon,
        GameLost,
        PacManIsInvincible,
        PacManIsNotInvincible,
        PacManEat,
        PacManLostLife,
        ScoreChanged,
        NbLifeRemainingChanged,
        NbGamesWonChanged
    };

    enum class PacManEatKind { Coin, Fruit, PasseMuraille, MapleSyrup, Ghost };

    GameplayEvent(const GameplayEvent&) = default;
    GameplayEvent()                     = delete;

    Kind kind() const { return kind_; }
    int score() const;
    int nbLifeRemaining() const;
    int nbLevelsFinished() const;

    PacManEatKind eatKind() const;
    entityx::Entity eatPacMan() const;
    entityx::Entity eatThing() const;

    static GameplayEvent newEvent(Kind kind);
    static GameplayEvent newScore(int score);
    static GameplayEvent newNbLifeRemaining(int nbLifeRemaining);
    static GameplayEvent newNbGamesWon(int nbGamesWon);
    static GameplayEvent newPacManEat(PacManEatKind kind, entityx::Entity pacman, entityx::Entity thing);

  private:
    GameplayEvent(Kind kind);
    GameplayEvent(Kind kind, int data);
    GameplayEvent(Kind kind, PacManEatKind eatKind, entityx::Entity eatPacman, entityx::Entity eatThing);

  private:
    const Kind kind_;

    struct Eat {
        const PacManEatKind eatKind;
        const entityx::Entity eatPacman;
        const entityx::Entity eatThing;

        Eat(PacManEatKind eatKind, entityx::Entity eatPacman, entityx::Entity eatThing);
    };

    union {
        const int dataInt;
        const Eat dataEat;
    };
};

struct CollisionOccurredEvent {
    const entityx::Entity e1, e2;

    CollisionOccurredEvent(const entityx::Entity& e1, const entityx::Entity& e2) : e1(e1), e2(e2) {}
};

struct TargetReached {
    const entityx::Entity entity;

    TargetReached(const entityx::Entity& entity) : entity(entity) {}
};

#endif // GL_PROJECT_2_ECS_EVENTS_HPP
