#ifndef GL_PROJECT_2_ECS_SYSTEMS_AUDIO_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_AUDIO_HPP

#include "gl-project-2/ecs/events.hpp"
#include <QHash>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QObject>
#include <QSoundEffect>
#include <entityx/entityx.h>

class Audio
: public QObject
, public entityx::System<Audio>
, public entityx::Receiver<Audio> {
  public:
    Audio(QObject* parent = nullptr);

    using entityx::System<Audio>::configure;
    void configure(entityx::EventManager& eventManager) override;

    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;

    void receive(const GameplayEvent& gameplayEvent);

    double ratioMusicImun = 1.0;

  private:
    QHash<QString, QSoundEffect*> soundEffects;
    QMediaPlayer* player;
    qint64 positionPlayerDefault;
};

#endif
