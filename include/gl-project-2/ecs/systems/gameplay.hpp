#ifndef GL_PROJECT_2_ECS_SYSTEMS_GAMEPLAY_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_GAMEPLAY_HPP

#include "gl-project-2/ecs/components.hpp"
#include "gl-project-2/ecs/events.hpp"
#include <entityx/entityx.h>

class Gameplay
: public entityx::System<Gameplay>
, public entityx::Receiver<Gameplay> {
  public:
    Gameplay() = default;

    using entityx::System<Gameplay>::configure;
    void configure(entityx::EntityManager& entities, entityx::EventManager& events) override;
    void init();
    void start();
    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;

    void receive(const CollisionOccurredEvent& collisionOccurredEvent);
    void receive(const GameplayEvent& gameplayEvent);
    void receive(const entityx::ComponentAddedEvent<MapleSyrup>& event);
    void receive(const entityx::ComponentRemovedEvent<MapleSyrup>& event);
    void receive(const entityx::ComponentAddedEvent<PasseMuraille>& event);
    void receive(const entityx::ComponentRemovedEvent<PasseMuraille>& event);
    void receive(const entityx::ComponentAddedEvent<GhostSoul>& event);
    void receive(const entityx::ComponentAddedEvent<RawSpeed>& event);
    void receive(const TargetReached& targetReached);

  private:
    entityx::EntityManager* entities = nullptr;
    entityx::EventManager* events    = nullptr;

    int score;
    int nbLifeRemaining;
    int nbGamesWon;
    double invincibleRemainingMsec;
    const double invincibleTotalRemainingMsec = 10000.0;
    int nbItemsEaten;
    int nbItemsToEat;
    const double MAPLE_SYRUP_TOTAL_REMAINING_MSEC    = 10000.0;
    const double PASSE_MURAILLE_TOTAL_REMAINING_MSEC = 10000.0;

    int scoreCoin;
    int scoreFruit;
    int scoreGhostBegin;
    int scoreGhostFactor;
    int scoreGhostCurrent;
    int bonusCost;
    float velocityFactor;

    bool isInvincible() const { return 0.0 < invincibleRemainingMsec; }

    void eat(const GameplayEvent& eat);
};

#endif
