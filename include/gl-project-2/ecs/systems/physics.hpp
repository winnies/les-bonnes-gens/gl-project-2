/*!
 * \file physics.hpp
 * \brief Physics engine
 * \author Laurent.D
 * \version 1.01
 * \date 23 novembre 2019
 */

#ifndef GL_PROJECT_2_ECS_SYSTEMS_PHYSICS_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_PHYSICS_HPP

#include "gl-project-2/ecs/components.hpp"
#include "gl-project-2/ecs/events.hpp"
#include <entityx/entityx.h>

/*! \class Physics
 * \brief Class Physics engine.
 *
 *  The class manages the physical engine.
 */
class Physics : public entityx::System<Physics> {
    /*! \publicsection */
  public:
    /*!
     *  \fn Constructor
     *
     *  \brief Constructor Physics engine.
     */
    Physics() = default;

    /*!
     * \fn update
     *
     * \brief function that will be recalled regularly for: - Move entities.
     *                                                      - Be informed if there are collisions between entities.
     */
    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;
};

#endif
