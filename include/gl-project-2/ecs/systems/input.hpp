#ifndef GL_PROJECT_2_ECS_SYSTEMS_INPUT_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_INPUT_HPP

#include "gl-project-2/ecs/components.hpp"
#include <QtWidgets>
#include <entityx/entityx.h>

class Input
: public QWidget
, public entityx::System<Input> {
    Q_OBJECT

  public:
    explicit Input(QWidget* parent = nullptr);

    using entityx::System<Input>::configure;
    void configure(entityx::EntityManager& entities, entityx::EventManager& events) override;
    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;

  protected:
    void keyPressEvent(QKeyEvent* event) override;

  private:
    entityx::EntityManager* entities = nullptr;
};

#endif
