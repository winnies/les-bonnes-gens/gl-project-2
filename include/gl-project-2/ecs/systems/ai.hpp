#ifndef GL_PROJECT_2_ECS_SYSTEMS_AI_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_AI_HPP

#include "gl-project-2/astar/grid.hpp"
#include "gl-project-2/ecs/components.hpp"
#include <QPoint>
#include <QSize>
#include <entityx/entityx.h>

class AI
: public entityx::System<AI>
, public entityx::Receiver<AI> {
  public:
    AI() = default;

    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;
    QPoint gridPos(const QPointF& coord) const;
    using entityx::System<AI>::configure;
    void configure(entityx::EntityManager& es, entityx::EventManager& events) override;
    void receive(const entityx::ComponentAddedEvent<ChameleonAI>& event);
    void receive(const entityx::ComponentRemovedEvent<RangeAI>& event);
    void receive(const entityx::ComponentRemovedEvent<ChameleonAI>& event);

  public:
    QSize mapSize;
    QSize tileSize;

  private:
    entityx::EntityManager* entities;
};

#endif // GL_PROJECT_2_ECS_SYSTEMS_AI_HPP
