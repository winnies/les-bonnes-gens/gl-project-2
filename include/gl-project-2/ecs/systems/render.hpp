#ifndef GL_PROJECT_2_ECS_SYSTEMS_RENDER_HPP
#define GL_PROJECT_2_ECS_SYSTEMS_RENDER_HPP

#include "include/gl-project-2/ecs/components.hpp"
#include <QGraphicsView>
#include <entityx/entityx.h>

class Render
: public QGraphicsView
, public entityx::System<Render> {
    Q_OBJECT

  public:
    explicit Render(QWidget* parent = nullptr);
    ~Render() override;

    Sprite addSprite(QVector<Sprite::Frame> frames, QSize size);

    using entityx::System<Render>::configure;
    void configure(entityx::EntityManager& entities, entityx::EventManager& events) override;
    void update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) override;

  protected:
    void resizeEvent(QResizeEvent* event) override;

  private:
    QGraphicsScene* scene;
    QRectF sceneRect;
    entityx::EntityManager* entities{};
};

extern Render* globalRender;

#endif
