#include "gl-project-2/aseprite.hpp"
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

QVector<Sprite::Frame> readAsepriteFrames(const QString& fileName) {
    QFile jsonFile(fileName);
    if (!jsonFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Couldn't open" << fileName;
        return {};
    }

    QJsonDocument loadDoc = QJsonDocument::fromJson(jsonFile.readAll());
    if (loadDoc.isNull()) {
        qWarning() << "Error while parsing" << fileName;
        return {};
    }

    QJsonObject dataObject = loadDoc.object();
    QJsonObject metaObject = dataObject[QStringLiteral("meta")].toObject();
    QString imageFileName  = metaObject[QStringLiteral("image")].toString();
    QJsonArray framesArray = dataObject[QStringLiteral("frames")].toArray();

    QDir fileDir      = QFileInfo(fileName).dir();
    QString imagePath = fileDir.filePath(imageFileName);
    QPixmap imagePixmap(imagePath);
    if (imagePixmap.isNull()) {
        qWarning() << "Couldn't load" << imagePath;
        return {};
    }

    QVector<Sprite::Frame> frames;
    for (QJsonValue frameValue : framesArray) {
        QJsonObject frameObject      = frameValue.toObject();
        double durationMsec          = frameObject[QStringLiteral("duration")].toDouble(-1.0);
        QJsonObject frameFrameObject = frameObject[QStringLiteral("frame")].toObject();
        int frameX                   = frameFrameObject[QStringLiteral("x")].toInt(-1);
        int frameY                   = frameFrameObject[QStringLiteral("y")].toInt(-1);
        int frameW                   = frameFrameObject[QStringLiteral("w")].toInt(-1);
        int frameH                   = frameFrameObject[QStringLiteral("h")].toInt(-1);

        if (durationMsec == -1.0 || frameX == -1 || frameY == -1 || frameW == -1 || frameH == -1) {
            qWarning() << "Error occurred while interpreting values of" << fileName;
            return {};
        }

        frames.push_back({durationMsec, imagePixmap.copy(frameX, frameY, frameW, frameH)});
    }

    return frames;
}
