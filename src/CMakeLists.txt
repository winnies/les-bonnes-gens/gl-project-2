add_subdirectory(ecs)
add_subdirectory(astar)

configure_file(application.cpp.in application.cpp)

target_sources(gl-project-2 PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/application.cpp aseprite.cpp level.cpp main.cpp)
