#include "gl-project-2/ecs/events.hpp"
#include <cassert>

int GameplayEvent::score() const {
    assert(kind_ == Kind::ScoreChanged);
    return dataInt;
}

int GameplayEvent::nbLifeRemaining() const {
    assert(kind_ == Kind::NbLifeRemainingChanged);
    return dataInt;
}

int GameplayEvent::nbLevelsFinished() const {
    assert(kind_ == Kind::NbGamesWonChanged);
    return dataInt;
}

GameplayEvent::PacManEatKind GameplayEvent::eatKind() const {
    assert(kind_ == Kind::PacManEat);
    return dataEat.eatKind;
}

entityx::Entity GameplayEvent::eatPacMan() const {
    assert(kind_ == Kind::PacManEat);
    return dataEat.eatPacman;
}

entityx::Entity GameplayEvent::eatThing() const {
    assert(kind_ == Kind::PacManEat);
    return dataEat.eatThing;
}

GameplayEvent GameplayEvent::newEvent(GameplayEvent::Kind kind) {
    switch (kind) {
    case Kind::PacManEat:
    case Kind::ScoreChanged:
    case Kind::NbLifeRemainingChanged:
    case Kind::NbGamesWonChanged:
        assert(false);
    default:
        return GameplayEvent(kind);
    }
}

GameplayEvent GameplayEvent::newScore(int score) {
    return GameplayEvent(Kind::ScoreChanged, score);
}

GameplayEvent GameplayEvent::newNbLifeRemaining(int nbLifeRemaining) {
    return GameplayEvent(Kind::NbLifeRemainingChanged, nbLifeRemaining);
}

GameplayEvent GameplayEvent::newNbGamesWon(int nbGamesWon) {
    return GameplayEvent(Kind::NbGamesWonChanged, nbGamesWon);
}

GameplayEvent GameplayEvent::newPacManEat(GameplayEvent::PacManEatKind kind, entityx::Entity pacman, entityx::Entity thing) {
    return GameplayEvent(Kind::PacManEat, kind, pacman, thing);
}

GameplayEvent::GameplayEvent(GameplayEvent::Kind kind) : kind_(kind) {}

GameplayEvent::GameplayEvent(GameplayEvent::Kind kind, int data) : kind_(kind), dataInt(data) {}

GameplayEvent::GameplayEvent(GameplayEvent::Kind kind, PacManEatKind eatKind, entityx::Entity eatPacman, entityx::Entity eatThing)
: kind_(kind), dataEat(eatKind, eatPacman, eatThing) {}

GameplayEvent::Eat::Eat(GameplayEvent::PacManEatKind eatKind, entityx::Entity eatPacman, entityx::Entity eatThing)
: eatKind(eatKind), eatPacman(eatPacman), eatThing(eatThing) {}
