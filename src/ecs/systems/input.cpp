#include "gl-project-2/ecs/systems/input.hpp"

Input::Input(QWidget* parent) : QWidget(parent) {}

void Input::update(entityx::EntityManager& /*es*/, entityx::EventManager& /*events*/, entityx::TimeDelta /*dt*/) {}

void Input::keyPressEvent(QKeyEvent* event) {
    entities->each<Direction, InputMode>([&](entityx::Entity /*entity*/, Direction& direction, InputMode& inputMode) {
        if (inputMode.leftKey == event->key()) {
            direction.futurDirection = Direction::TypeDirection::Left;
        } else if (inputMode.upKey == event->key()) {
            direction.futurDirection = Direction::TypeDirection::Up;
        } else if (inputMode.rightKey == event->key()) {
            direction.futurDirection = Direction::TypeDirection::Right;
        } else if (inputMode.downKey == event->key()) {
            direction.futurDirection = Direction::TypeDirection::Down;
        }
    });
}

void Input::configure(entityx::EntityManager& entities, entityx::EventManager& /*events*/) {
    this->entities = &entities;
}
