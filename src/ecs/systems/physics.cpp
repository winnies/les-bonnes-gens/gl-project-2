/*!
 * \file physics.cpp
 * \brief Physics engine
 * \author Laurent.D
 * \version 1.1
 * \date 1 décembre 2019
 */
#include "gl-project-2/ecs/systems/physics.hpp"
#include "gl-project-2/ecs/events.hpp"

using TD = Direction::TypeDirection;

/*!
========================================================================================
===============================Part Function Declarations===============================
========================================================================================
*/

/*!
 * \fn collisionEntity
 * \brief Function called to know if there is a collision between two entities.
 *
 * \param myBox Box of an entity.
 * \param otherBox Box of an entity.
 * \return Returns a boolean, TRUE if there is a collision | FALSE if there is no collision.
 *
 */
bool collisionEntity(const QRectF& myBox, const QRectF& otherBox);

/*!
 * \fn correctPosEntity
 *
 * \brief Function called to correct the position of an entity, if there was a collision between two entities.
 *
 * \param pos Position of the entity has moved.
 * \param direction Direction of the entity has moved.
 * \param box Box of the entity has moved.
 * \param otherBox Box of another entity ( We will use this box to move the entity according to this box).
 *
 */
void correctPosEntity(Position& pos, const TD& direction, const QRectF& box, const QRectF& otherBox);

/*!
 * \fn movePosition
 * \brief Function called to move the entity to one position to another position.
 *
 * \param pos Position of the entity.
 * \param direction Direction of the entity.
 * \param move The move he has to make
 *
 */
void movePosition(Position& pos, const TD& direction, const double& move);

/*!
 * \fn nextMove
 * \brief Function called to know the move he has to make.
 *
 * \param pos Position of the entity.
 * \param direction Direction of the entity.
 * \param move The move he has to make
 * \return Returns a double, this is the number used to move it.
 *
 */
double nextMove(const Position& actualPos, const Position& nextPos, const TD& direction);

/*!
 * \fn listEntitiesCollision
 * \brief Function called to know the number of entities that may have a collision with my entity.
 *
 * \param myEntity  entity.
 * \param myBox Box of the entity.
 * \param es To check all entities.
 * \return Returns a QSet of entities that will collisions with my entity.
 *
 */
QSet<entityx::Entity> listEntitiesCollision(const entityx::Entity& myEntity, const QRectF& myBox, entityx::EntityManager& es);

/*!
============================================================================
===============================Part Physics===============================
============================================================================
*/
void Physics::update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) {
    es.each<Position, Direction, Velocity, CollisionBoxes>(
        [&](entityx::Entity entity, Position& position, Direction& direction, Velocity& velocity, CollisionBoxes& boxes) {
            if (!exists(entity)) {
                return;
            }

            auto move{velocity * dt};

            QSet<entityx::Entity> listEntities{listEntitiesCollision(
                entity, QRectF(position - QPointF(move, move), CollisionBoxes::plusScaleBox(boxes.globalSize, move * 2)), es)};

            if (direction.different()) {
                if (direction.differentAxe()) {
                    Position testPos  = position.toPoint();
                    Position newPos   = testPos;
                    auto incrementPos = 0;

                    movePosition(testPos, direction.futurDirection, 1.0);

                    for (auto i = 0; i <= move; i++) {
                        auto findIntersection = true;

                        movePosition(testPos, direction.actualDirection, incrementPos);
                        movePosition(newPos, direction.actualDirection, incrementPos);

                        for (auto myBox : boxes.boxes) {
                            for (auto otherEntity : listEntities) {
                                entityx::ComponentHandle<CollisionBoxes> otherBoxes = otherEntity.component<CollisionBoxes>();
                                entityx::ComponentHandle<Position> otherPosition    = otherEntity.component<Position>();
                                if (boxes.mask.contains(otherBoxes->getLayer())) {
                                    for (auto otherBox : otherBoxes->boxes) {
                                        if (collisionEntity(myBox.translated(testPos),
                                                            otherBox.translated(otherPosition->toPoint()))) {
                                            findIntersection = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (incrementPos == 0 && findIntersection) {
                            direction.actualDirection = direction.futurDirection;
                            position                  = newPos;
                            break;
                        }
                        if (incrementPos == 0) {
                            incrementPos = 1;
                        }

                        if (findIntersection) {
                            move = nextMove(position, newPos, direction.actualDirection);
                            break;
                        }
                    }
                } else {
                    direction.actualDirection = direction.futurDirection;
                }
            }

            movePosition(position, direction.actualDirection, move);

            for (auto myBox : boxes.boxes) {
                for (auto otherEntity : listEntities) {
                    entityx::ComponentHandle<CollisionBoxes> otherBoxes = otherEntity.component<CollisionBoxes>();
                    entityx::ComponentHandle<Position> otherPosition    = otherEntity.component<Position>();
                    for (auto otherBox : otherBoxes->boxes) {
                        if (collisionEntity(myBox.translated(position), otherBox.translated(otherPosition->toPoint()))) {
                            if (boxes.mask.contains(otherBoxes->getLayer())) {
                                correctPosEntity(
                                    position, direction.actualDirection, myBox, otherBox.translated(otherPosition->toPoint()));
                            }
                            events.emit<CollisionOccurredEvent>(entity, otherEntity);
                        }
                    }
                }
            }
        });
}

/*!
======================================================================================
===============================Part Defining a Function===============================
======================================================================================
*/

bool collisionEntity(const QRectF& myBox, const QRectF& otherBox) {
    return otherBox.intersects(myBox);
}

void correctPosEntity(Position& pos, const TD& direction, const QRectF& box, const QRectF& otherBox) {
    switch (direction) {
    case TD::Up:
        pos.ry() = otherBox.bottomLeft().y();
        break;
    case TD::Down:
        pos.ry() = otherBox.topLeft().y() - box.height();
        break;
    case TD::Left:
        pos.rx() = otherBox.topRight().x();
        break;
    case TD::Right:
        pos.rx() = otherBox.topLeft().x() - box.width();
        break;
    }
}

void movePosition(Position& pos, const TD& direction, const double& move) {
    switch (direction) {
    case TD::Up:
        pos.ry() -= move;
        break;
    case TD::Down:
        pos.ry() += move;
        break;
    case TD::Left:
        pos.rx() -= move;
        break;
    case TD::Right:
        pos.rx() += move;
        break;
    }
}

double nextMove(const Position& actualPos, const Position& nextPos, const TD& direction) {
    double move;
    switch (direction) {
    case TD::Up:
        move = actualPos.y() - nextPos.y();
        break;
    case TD::Down:
        move = nextPos.y() - actualPos.y();
        break;
    case TD::Left:
        move = actualPos.x() - nextPos.x();
        break;
    case TD::Right:
        move = nextPos.x() - actualPos.x();
        break;
    }
    return move;
}

QSet<entityx::Entity> listEntitiesCollision(const entityx::Entity& myEntity, const QRectF& myBox, entityx::EntityManager& es) {
    QSet<entityx::Entity> listEntities{};

    es.each<Position, CollisionBoxes>([&](entityx::Entity otherEntity, Position& otherPosition, CollisionBoxes& otherBoxes) {
        if (myEntity == otherEntity || !exists(otherEntity)) {
            return;
        }

        for (auto otherBox : otherBoxes.boxes) {
            if (collisionEntity(myBox, otherBox.translated(otherPosition))) {
                listEntities.insert(otherEntity);
                break;
            }
        }
    });

    return listEntities;
}
