#include "gl-project-2/ecs/systems/gameplay.hpp"

using GE = GameplayEvent;
using GK = GameplayEvent::Kind;

void ghostSoulApplyState(entityx::Entity entity, GhostSoul::State state) {
    GhostSoul& ghostSoul = *entity.component<GhostSoul>();
    using S              = GhostSoul::State;

    switch (ghostSoul.state = state) {
    case S::Waiting:
        ghostSoul.remainingMsec                       = ghostSoul.waitingMsec;
        entity.component<Direction>()->futurDirection = Direction::TypeDirection::Up;
        break;
    case S::ComingOut:
        entity.replace<SmartAI>();
        entity.replace<Target>(ghostSoul.outside);
        break;
    case S::Hungry:
        switch (ghostSoul.color) {
        case GhostSoul::Color::Green:
            entity.replace<RandomAI>();
            break;
        case GhostSoul::Color::Yellow:
            entity.replace<RangeAI>();
            break;
        case GhostSoul::Color::Red:
            entity.replace<SmartAI>();
            break;
        case GhostSoul::Color::Blue:
            entity.replace<ChameleonAI>();
            break;
        }
        entity.replace<Target>(ghostSoul.pacman);
        break;
    case S::Fearful:
        entity.replace<RandomAI>(1);
        entity.replace<DirectionToFramesMap>(ghostSoul.fearfulFrames);
        break;
    case S::Loser:
        entity.replace<SmartAI>();
        entity.replace<Target>(entity.component<Spawn>()->position);
        entity.replace<DirectionToFramesMap>(ghostSoul.loserFrames);
        break;
    }
}

void ghostSoulChangeState(entityx::Entity entity, GhostSoul::State newState) {
    GhostSoul& ghostSoul = *entity.component<GhostSoul>();
    using S              = GhostSoul::State;

    switch (ghostSoul.state) {
    case S::Waiting:
        break;
    case S::ComingOut:
        if (entity.has_component<SmartAI>()) {
            entity.remove<SmartAI>();
        }
        break;
    case S::Hungry:
        switch (ghostSoul.color) {
        case GhostSoul::Color::Green:
            if (entity.has_component<RandomAI>()) {
                entity.remove<RandomAI>();
            }
            break;
        case GhostSoul::Color::Yellow:
            if (entity.has_component<RangeAI>()) {
                entity.remove<RangeAI>();
            }
            break;
        case GhostSoul::Color::Red:
            if (entity.has_component<SmartAI>()) {
                entity.remove<SmartAI>();
            }
            break;
        case GhostSoul::Color::Blue:
            if (entity.has_component<ChameleonAI>()) {
                entity.remove<ChameleonAI>();
            }
            break;
        }
        break;
    case S::Fearful:
        if (entity.has_component<RandomAI>()) {
            entity.remove<RandomAI>();
        }
        entity.replace<DirectionToFramesMap>(ghostSoul.normalFrames);
        break;
    case S::Loser:
        if (entity.has_component<SmartAI>()) {
            entity.remove<SmartAI>();
        }
        entity.replace<DirectionToFramesMap>(ghostSoul.normalFrames);
        break;
    }
    ghostSoulApplyState(entity, newState);
}

void Gameplay::configure(entityx::EntityManager& entities, entityx::EventManager& events) {
    this->entities = &entities;
    this->events   = &events;
    events.subscribe<CollisionOccurredEvent>(*this);
    events.subscribe<GameplayEvent>(*this);

    events.subscribe<entityx::ComponentAddedEvent<MapleSyrup>>(*this);
    events.subscribe<entityx::ComponentRemovedEvent<MapleSyrup>>(*this);

    events.subscribe<entityx::ComponentAddedEvent<PasseMuraille>>(*this);
    events.subscribe<entityx::ComponentRemovedEvent<PasseMuraille>>(*this);

    events.subscribe<entityx::ComponentAddedEvent<GhostSoul>>(*this);
    events.subscribe<entityx::ComponentAddedEvent<RawSpeed>>(*this);
    events.subscribe<TargetReached>(*this);
}

void Gameplay::init() {
    score            = 0;
    nbLifeRemaining  = 3;
    nbGamesWon       = 0;
    nbItemsToEat     = 0;
    scoreCoin        = 10;
    scoreFruit       = 50;
    scoreGhostBegin  = 200;
    scoreGhostFactor = 2;
    velocityFactor   = 1.1F;
    bonusCost        = 200;

    entities->each<Role>([&](entityx::Entity /*entity*/, Role& role) {
        switch (role) {
        case Role::Coin:
        case Role::Fruit:
            ++nbItemsToEat;
            break;
        default:
            break;
        }
    });

    events->emit<GE>(GE::newScore(score));
    events->emit<GE>(GE::newNbLifeRemaining(nbLifeRemaining));
    events->emit<GE>(GE::newNbGamesWon(nbGamesWon));
}

void Gameplay::start() {
    events->emit<GE>(GE::newEvent(GK::GameStarted));
}

void Gameplay::update(entityx::EntityManager& /*es*/, entityx::EventManager& events, entityx::TimeDelta dt) {
    if (isInvincible()) {
        invincibleRemainingMsec = std::max(invincibleRemainingMsec - dt, 0.0);

        if (!isInvincible()) {
            events.emit<GE>(GE::newEvent(GK::PacManIsNotInvincible));
        }
    }

    entities->each<PasseMuraille>([&](entityx::Entity entity, PasseMuraille& passeMuraille) {
        passeMuraille.remainingMsec -= dt;

        if (passeMuraille.remainingMsec <= 0.0) {
            entity.remove<PasseMuraille>();
        }
    });

    entities->each<MapleSyrup>([&](entityx::Entity entity, MapleSyrup& mapleSyrup) {
        mapleSyrup.remainingMsec -= dt;

        if (mapleSyrup.remainingMsec <= 0.0) {
            entity.remove<MapleSyrup>();
        }
    });

    entities->each<GhostSoul>([&](entityx::Entity entity, GhostSoul& ghostSoul) {
        switch (ghostSoul.state) {
        case GhostSoul::State::Waiting:
            ghostSoul.remainingMsec -= dt;
            if (ghostSoul.remainingMsec <= 0.0) {
                ghostSoulChangeState(entity, GhostSoul::State::ComingOut);
            }
            break;
        default:
            break;
        }
    });
}

void Gameplay::receive(const CollisionOccurredEvent& collisionOccurredEvent) {
    entityx::Entity e1 = collisionOccurredEvent.e1;
    entityx::Entity e2 = collisionOccurredEvent.e2;

    auto is = [&e1, &e2](Role r1, Role r2) {
        if (!exists(e1) || !exists(e2)) {
            return false;
        }

        auto c1 = e1.component<Role>();
        auto c2 = e2.component<Role>();
        if (!c1 || !c2) {
            return false;
        }
        if (*c1 == r1 && *c2 == r2) {
            return true;
        }
        if (*c1 == r2 && *c2 == r1) {
            std::swap(e1, e2);
            return true;
        }
        return false;
    };

    if (is(Role::PacMan, Role::Coin)) {
        events->emit<GE>(GE::newPacManEat(GE::PacManEatKind::Coin, e1, e2));
    } else if (is(Role::PacMan, Role::Fruit)) {
        events->emit<GE>(GE::newPacManEat(GE::PacManEatKind::Fruit, e1, e2));
    } else if (is(Role::PacMan, Role::PasseMuraille)) {
        events->emit<GE>(GE::newPacManEat(GE::PacManEatKind::PasseMuraille, e1, e2));
    } else if (is(Role::PacMan, Role::MapleSyrup)) {
        events->emit<GE>(GE::newPacManEat(GE::PacManEatKind::MapleSyrup, e1, e2));
    } else if (is(Role::PacMan, Role::Ghost)) {
        auto state = e2.component<GhostSoul>()->state;
        using S    = GhostSoul::State;

        if (isInvincible() && state == S::Fearful) {
            events->emit<GE>(GE::newPacManEat(GE::PacManEatKind::Ghost, e1, e2));
        }

        if (state == S::Hungry || state == S::ComingOut || state == S::Waiting) {
            nbLifeRemaining -= 1;

            events->emit<GE>(GE::newNbLifeRemaining(nbLifeRemaining));
            events->emit<GE>(GE::newEvent(GK::PacManLostLife));
        }
    }
}

void Gameplay::receive(const GameplayEvent& gameplayEvent) {
    switch (gameplayEvent.kind()) {
    case GK::PacManEat:
        eat(gameplayEvent);
        break;
    case GK::PacManIsInvincible:
        scoreGhostCurrent = scoreGhostBegin;

        entities->each<GhostSoul>([&](entityx::Entity entity, GhostSoul& ghostSoul) {
            switch (ghostSoul.state) {
            case GhostSoul::State::Hungry:
                ghostSoulChangeState(entity, GhostSoul::State::Fearful);
                break;
            default:
                break;
            }
        });
        break;
    case GK::PacManIsNotInvincible:
        entities->each<GhostSoul>([&](entityx::Entity entity, GhostSoul& ghostSoul) {
            switch (ghostSoul.state) {
            case GhostSoul::State::Fearful:
                ghostSoulChangeState(entity, GhostSoul::State::Hungry);
                break;
            default:
                break;
            }
        });
        break;
    case GK::PacManLostLife:
        if (nbLifeRemaining > 0) {
            entities->each<Position, Spawn>(
                [&](entityx::Entity /*entity*/, Position& position, Spawn& spawn) { position = spawn.position; });
            entities->each<MapleSyrup>([&](entityx::Entity entity, MapleSyrup& /*mapleSyrup*/) { entity.remove<MapleSyrup>(); });
            entities->each<PasseMuraille>(
                [&](entityx::Entity entity, PasseMuraille& /*passeMuraille*/) { entity.remove<PasseMuraille>(); });
            entities->each<GhostSoul>([&](entityx::Entity entity, GhostSoul& /*ghostSoul*/) {
                ghostSoulChangeState(entity, GhostSoul::State::Waiting);
            });
        } else {
            events->emit<GE>(GE::newEvent(GK::GameLost));
        }
        break;
    case GK::GameStarted:
        invincibleRemainingMsec = 0.0;
        nbItemsEaten            = 0;
        scoreGhostCurrent       = scoreGhostBegin;

        entities->each<PasseMuraille>(
            [&](entityx::Entity entity, PasseMuraille& /*passeMuraille*/) { entity.remove<PasseMuraille>(); });

        entities->each<MapleSyrup>([&](entityx::Entity entity, MapleSyrup& /*mapleSyrup*/) { entity.remove<MapleSyrup>(); });

        if (nbGamesWon > 0) {
            entities->each<Velocity>([&](entityx::Entity /*entity*/, Velocity& velocity) { velocity *= velocityFactor; });
            scoreCoin *= 2;
            scoreFruit *= 2;
            bonusCost *= 2;
        }

        entities->each<Exists>([&](entityx::Entity /*entity*/, Exists& exists) { exists.exists = true; });
        entities->each<Position, Spawn>(
            [&](entityx::Entity /*entity*/, Position& position, Spawn& spawn) { position = spawn.position; });
        entities->each<GhostSoul>(
            [&](entityx::Entity entity, GhostSoul& /*ghostSoul*/) { ghostSoulChangeState(entity, GhostSoul::State::Waiting); });
        break;
    case GK::GameEnded:
        break;
    case GK::GameWon:
        nbGamesWon += 1;
        events->emit<GE>(GE::newNbGamesWon(nbGamesWon));
        events->emit<GE>(GE::newEvent(GK::GameEnded));
        events->emit<GE>(GE::newEvent(GK::GameStarted));
        break;
    case GK::GameLost:
        entities->each<Velocity, RawSpeed>(
            [&](entityx::Entity /*entity*/, Velocity& velocity, RawSpeed& rawSpeed) { velocity = rawSpeed.speed; });
        events->emit<GE>(GE::newEvent(GK::GameEnded));
        break;
    default:
        break;
    }
}

void Gameplay::receive(const entityx::ComponentAddedEvent<MapleSyrup>& event) {
    auto entity = event.entity;
    for (QRectF& box : entity.component<CollisionBoxes>()->boxes) {
        box.setSize(box.size() / 2);
    }
    entity.component<Sprite>()->size /= 2;
}

void Gameplay::receive(const entityx::ComponentRemovedEvent<MapleSyrup>& event) {
    auto entity = event.entity;
    for (QRectF& box : entity.component<CollisionBoxes>()->boxes) {
        box.setSize(box.size() * 2);
    }
    entity.component<Sprite>()->size *= 2;
}

void Gameplay::receive(const entityx::ComponentAddedEvent<PasseMuraille>& event) {
    auto entity = event.entity;
    entity.component<CollisionBoxes>()->mask.remove("Wall");
    entity.component<CollisionBoxes>()->mask.remove("Door");
}

void Gameplay::receive(const entityx::ComponentRemovedEvent<PasseMuraille>& event) {
    auto entity = event.entity;
    entity.component<CollisionBoxes>()->mask.insert("Wall");
    entity.component<CollisionBoxes>()->mask.insert("Door");
}

void Gameplay::receive(const entityx::ComponentAddedEvent<GhostSoul>& event) {
    auto entity    = event.entity;
    auto ghostSoul = *event.component;
    ghostSoulApplyState(entity, ghostSoul.state);
}

void Gameplay::receive(const entityx::ComponentAddedEvent<RawSpeed>& event) {
    auto entity = event.entity;
    auto speed  = entity.component<RawSpeed>()->speed;
    entity.assign<Velocity>(speed);
}

void Gameplay::receive(const TargetReached& targetReached) {
    auto entity    = targetReached.entity;
    auto ghostSoul = entity.component<GhostSoul>();
    if (!ghostSoul) {
        return;
    }
    switch (ghostSoul->state) {
    case GhostSoul::State::ComingOut:
        ghostSoulChangeState(entity, GhostSoul::State::Hungry);
        break;
    case GhostSoul::State::Loser:
        ghostSoulChangeState(entity, GhostSoul::State::Waiting);
        break;
    default:
        break;
    }
}

void Gameplay::eat(const GameplayEvent& eat) {
    using K = GameplayEvent::PacManEatKind;

    switch (eat.eatKind()) {
    case K::Coin:
        eat.eatThing().component<Exists>()->exists = false;
        score += scoreCoin;
        break;
    case K::Fruit:
        eat.eatThing().component<Exists>()->exists = false;
        score += scoreFruit;
        invincibleRemainingMsec = invincibleTotalRemainingMsec;
        events->emit<GE>(GE::newEvent(GK::PacManIsInvincible));
        break;
    case K::Ghost:
        ghostSoulChangeState(eat.eatThing(), GhostSoul::State::Loser);
        score += scoreGhostCurrent;
        scoreGhostCurrent *= scoreGhostFactor;
        break;
    case K::MapleSyrup:
        eat.eatPacMan().replace<MapleSyrup>(MAPLE_SYRUP_TOTAL_REMAINING_MSEC);
        eat.eatThing().component<Exists>()->exists = false;
        score -= bonusCost;
        break;
    case K::PasseMuraille:
        eat.eatPacMan().replace<PasseMuraille>(PASSE_MURAILLE_TOTAL_REMAINING_MSEC);
        eat.eatThing().component<Exists>()->exists = false;
        score -= bonusCost;
        break;
    }

    events->emit<GE>(GE::newScore(score));

    switch (eat.eatKind()) {
    case K::Coin:
    case K::Fruit:
        nbItemsEaten += 1;
        break;
    default:
        break;
    }

    if (nbItemsEaten == nbItemsToEat) {
        events->emit<GE>(GE::newEvent(GK::GameWon));
    }
}
