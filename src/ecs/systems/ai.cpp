﻿#include "gl-project-2/ecs/systems/ai.hpp"
#include "gl-project-2/astar/astar.hpp"
#include "gl-project-2/ecs/events.hpp"
#include "gl-project-2/ecs/systems/render.hpp"
#include <QRandomGenerator>
#include <QVector2D>

QPointF getTargetPos(const Target& target) {
    switch (target.kind()) {
    case Target::Kind::Entity:
        return *target.entity().component<Position>();
    case Target::Kind::Position:
        return target.position();
    }
    return {};
}

void AI::update(entityx::EntityManager& es, entityx::EventManager& events, entityx::TimeDelta dt) {
    es.each<Direction, RandomAI>([&](entityx::Entity /*entity*/, Direction& direction, RandomAI& randomAI) {
        randomAI.elapsedTime += dt;

        if (randomAI.elapsedTime < randomAI.decisionTime) {
            return;
        }

        int rand = QRandomGenerator::global()->bounded(4);

        switch (rand) {
        case 0:
            direction.futurDirection = Direction::TypeDirection::Left;
            break;
        case 1:
            direction.futurDirection = Direction::TypeDirection::Up;
            break;
        case 2:
            direction.futurDirection = Direction::TypeDirection::Right;
            break;
        case 3:
            direction.futurDirection = Direction::TypeDirection::Down;
            break;
        }

        randomAI.elapsedTime -= randomAI.decisionTime;
    });

    es.each<Position, Target, RangeAI>([&](entityx::Entity entity, Position& position, Target& target, RangeAI& rangeAI) {
        QPointF targetPos(getTargetPos(target));

        if (QVector2D(targetPos - position).length() < rangeAI.range) {
            if (entity.has_component<SmartAI>()) {
                return;
            }
            if (entity.has_component<RandomAI>()) {
                entity.remove<RandomAI>();
            }
            entity.assign<SmartAI>();
        } else {
            if (entity.has_component<RandomAI>()) {
                return;
            }
            if (entity.has_component<SmartAI>()) {
                entity.remove<SmartAI>();
            }
            entity.assign<RandomAI>(3000);
        }
    });

    es.each<Direction, Target, Position, SmartAI>(
        [&](entityx::Entity entity, Direction& direction, Target& target, Position& position, SmartAI& /*smartAI*/) {
            if (entity.component<SmartAI>()->grid.lignNumber == -1 || entity.component<SmartAI>()->grid.columnNumber == -1) {
                QVector<bool> boules(mapSize.width() * mapSize.height(), false);

                entities->each<Position, GhostPath>(
                    [&](entityx::Entity /*entity*/, Position& position, GhostPath& /*ghostPath*/) {
                        QPoint tmp                                  = gridPos(position);
                        boules[tmp.x() * mapSize.width() + tmp.y()] = true;
                    });

                entity.component<SmartAI>()->grid = Grid(boules, mapSize.width());
            }

            Astar algo(entity.component<SmartAI>()->grid);

            QPoint currentPos    = gridPos(position);
            QVector<QPoint> path = algo.findPath(currentPos, gridPos(getTargetPos(target)));

            if (path.empty()) {
                return;
            } else if (path.size() == 1) {
                events.emit<TargetReached>(entity);
            } else {
                QPoint nextPos = path.at(path.size() - 2);
                Direction::TypeDirection actualDir;

                if (nextPos.x() < currentPos.x()) {
                    actualDir = Direction::TypeDirection::Up;
                } else if (nextPos.x() > currentPos.x()) {
                    actualDir = Direction::TypeDirection::Down;
                } else if (nextPos.y() < currentPos.y()) {
                    actualDir = Direction::TypeDirection::Left;
                } else if (nextPos.y() > currentPos.y()) {
                    actualDir = Direction::TypeDirection::Right;
                } else {
                    return;
                }

                if (actualDir != direction.getActualDirection()) {
                    direction.futurDirection = actualDir;
                    return;
                }

                if (path.size() > 2) {
                    QPoint nextNextPos = path.at(path.size() - 3);

                    if (nextNextPos.x() < nextPos.x()) {
                        direction.futurDirection = Direction::TypeDirection::Up;
                    } else if (nextNextPos.x() > nextPos.x()) {
                        direction.futurDirection = Direction::TypeDirection::Down;
                    } else if (nextNextPos.y() < nextPos.y()) {
                        direction.futurDirection = Direction::TypeDirection::Left;
                    } else if (nextNextPos.y() > nextPos.y()) {
                        direction.futurDirection = Direction::TypeDirection::Right;
                    }
                }
            }
        });

    es.each<ChameleonAI>([&](entityx::Entity entity, ChameleonAI& chameleonAI) {
        chameleonAI.elapsedTime += dt;

        if (chameleonAI.elapsedTime < chameleonAI.shiftTime) {
            return;
        }

        if (entity.has_component<RandomAI>()) {
            entity.remove<RandomAI>();
        }
        if (entity.has_component<RangeAI>()) {
            entity.remove<RangeAI>();
        }
        if (entity.has_component<SmartAI>()) {
            entity.remove<SmartAI>();
        }

        int rand = QRandomGenerator::global()->bounded(3);

        switch (rand) {
        case 0:
            entity.assign<RandomAI>();
            break;
        case 1:
            entity.assign<RangeAI>(8 * tileSize.width());
            break;
        case 2:
            entity.assign<SmartAI>();
            break;
        }

        chameleonAI.elapsedTime -= chameleonAI.shiftTime;
    });
}

QPoint AI::gridPos(const QPointF& coord) const {
    return {(static_cast<int>(coord.y())) / tileSize.height(), static_cast<int>(coord.x()) / tileSize.width()};
}

void AI::configure(entityx::EntityManager& es, entityx::EventManager& events) {
    this->entities = &es;
    events.subscribe<entityx::ComponentAddedEvent<ChameleonAI>>(*this);
    events.subscribe<entityx::ComponentRemovedEvent<RangeAI>>(*this);
    events.subscribe<entityx::ComponentRemovedEvent<ChameleonAI>>(*this);
}

void AI::receive(const entityx::ComponentAddedEvent<ChameleonAI>& event) {
    auto entity = event.entity;
    entity.replace<RandomAI>();
}

void AI::receive(const entityx::ComponentRemovedEvent<RangeAI>& event) {
    auto entity = event.entity;

    if (entity.has_component<RandomAI>()) {
        entity.remove<RandomAI>();
    } else if (entity.has_component<SmartAI>()) {
        entity.remove<SmartAI>();
    }
}

void AI::receive(const entityx::ComponentRemovedEvent<ChameleonAI>& event) {
    auto entity = event.entity;

    if (entity.has_component<RandomAI>()) {
        entity.remove<RandomAI>();
    } else if (entity.has_component<SmartAI>()) {
        entity.remove<SmartAI>();
    } else if (entity.has_component<RangeAI>()) {
        entity.remove<RangeAI>();
    }
}
