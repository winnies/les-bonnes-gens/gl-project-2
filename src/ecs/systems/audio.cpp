#include "gl-project-2/ecs/systems/audio.hpp"
#include <QFile>

using namespace std;

Audio::Audio(QObject* parent) : QObject(parent) {
    positionPlayerDefault = 0;

    auto crunchEffect = new QSoundEffect(this);
    crunchEffect->setSource(QUrl(QStringLiteral("rs:sounds/crunch1.wav")));
    crunchEffect->setVolume(.7);
    soundEffects.insert("PacManEatCoin", crunchEffect);

    auto deathEffect = new QSoundEffect(this);
    deathEffect->setSource(QUrl(QStringLiteral("rs:sounds/death.wav")));
    deathEffect->setVolume(.7);
    soundEffects.insert("PacManLostLife", deathEffect);

    auto tesNulEffect = new QSoundEffect(this);
    tesNulEffect->setSource(QUrl(QStringLiteral("rs:sounds/tesnul.wav")));
    tesNulEffect->setVolume(.7);
    soundEffects.insert("GameLost", tesNulEffect);

    auto winEffect = new QSoundEffect(this);
    winEffect->setSource(QUrl(QStringLiteral("rs:sounds/win.wav")));
    winEffect->setVolume(.7);
    soundEffects.insert("GameWon", winEffect);

    // Loading soundtracks

    player = new QMediaPlayer(this);
}

void Audio::configure(entityx::EventManager& eventManager) {
    eventManager.subscribe<GameplayEvent>(*this);
}

void Audio::update(entityx::EntityManager& /*es*/, entityx::EventManager& /*events*/, entityx::TimeDelta /*dt*/) {
    switch (player->state()) {
    case QMediaPlayer::State::StoppedState:
        player->setMedia(QUrl(QStringLiteral("qrc:/sounds/musique_acc2.ogg")));
        player->play();
        player->setPlaybackRate(1.0);
        player->setPosition(positionPlayerDefault);
        positionPlayerDefault = 0;
        break;
    default:
        break;
    }
}

void Audio::receive(const GameplayEvent& gameplayEvent) {
    QSoundEffect* effect;
    switch (gameplayEvent.kind()) {
    case GameplayEvent::Kind::GameStarted:
        player->setMedia(QUrl(QStringLiteral("qrc:/sounds/musique_acc2.ogg")));
        player->play();
        player->setVolume(100);
        break;
    case GameplayEvent::Kind::PacManEat:
        switch (gameplayEvent.eatKind()) {
        case GameplayEvent::PacManEatKind::Coin:
            effect = soundEffects.value("PacManEatCoin");
            effect->play();
            break;
        case GameplayEvent::PacManEatKind::Fruit:
            player->pause();
            positionPlayerDefault = player->position();
            player->setMedia(QUrl(QStringLiteral("qrc:/sounds/Hisoka_acc2.ogg")));
            player->setPlaybackRate(ratioMusicImun);
            player->play();
            break;
        default:
            break;
        }
        break;
    case GameplayEvent::Kind::GameWon:
        effect = soundEffects.value("GameWon");
        player->setPlaybackRate(1.0);
        effect->play();
        break;
    case GameplayEvent::Kind::PacManLostLife:
        effect = soundEffects.value("PacManLostLife");
        effect->play();
        break;
    case GameplayEvent::Kind::GameLost:
        effect = soundEffects.value("GameLost");
        effect->play();
        break;
    case GameplayEvent::Kind::GameEnded:
        player->setPlaybackRate(1.0);
        break;
    default:
        break;
    }
}
