#include "gl-project-2/ecs/systems/render.hpp"
#include "gl-project-2/ecs/components.hpp"
#include <QOpenGLWidget>

Render* globalRender;

Render::Render(QWidget* parent) : QGraphicsView(parent) {
    scene = new QGraphicsScene(this);
    scene->setBackgroundBrush(Qt::black);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setScene(scene);
    setInteractive(false);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFrameStyle(QFrame::NoFrame);
    setViewport(new QOpenGLWidget);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing);
    setFocusPolicy(Qt::NoFocus);

    globalRender = this;
}

Render::~Render() {
    entities->each<Sprite>([&](entityx::Entity /*entity*/, Sprite& sprites) { scene->removeItem(sprites.pixmapItem.get()); });
}

Sprite Render::addSprite(QVector<Sprite::Frame> frames, QSize size) {
    Sprite sprite(frames, 0, size);
    scene->addItem(sprite.pixmapItem.get());
    return sprite;
}

void Render::configure(entityx::EntityManager& entities, entityx::EventManager& /*events*/) {
    this->entities = &entities;
}
#include <QDebug>
void Render::update(entityx::EntityManager& es, entityx::EventManager& /*events*/, entityx::TimeDelta dt) {
    es.each<Direction, Sprite, DirectionToFramesMap>(
        [&](entityx::Entity /*entity*/, Direction& direction, Sprite& sprite, DirectionToFramesMap& directionToFramesMap) {
            sprite.frames = directionToFramesMap.frames[direction.getActualDirection()];
            sprite.currentFrame %= sprite.frames.size();
        });

    es.each<Position, Sprite>([&](entityx::Entity entity, Position& position, Sprite& sprite) {
        sprite.pixmapItem->setVisible(exists(entity));
        if (!sprite.pixmapItem->isVisible() || sprite.frames.isEmpty()) {
            return;
        }

        sprite.pixmapItem->setPos(position);

        if (sprite.frames.size() > 1) {
            sprite.elapsedTimeMsec += dt;
            auto duration = sprite.frames.at(sprite.currentFrame).durationMsec;
            if (sprite.elapsedTimeMsec > duration) {
                sprite.elapsedTimeMsec -= duration;
                sprite.currentFrame = (sprite.currentFrame + 1) % sprite.frames.size();
            }
        }

        const Sprite::Frame& frame = sprite.frames.at(sprite.currentFrame);
        sprite.pixmapItem->setPixmap(frame.pixmap.scaled(sprite.size));
    });

    auto realSceneRect = scene->sceneRect();
    if (sceneRect != realSceneRect) {
        sceneRect = realSceneRect;
        fitInView(sceneRect, Qt::KeepAspectRatio);
    }
}

void Render::resizeEvent(QResizeEvent* /*event*/) {
    fitInView(sceneRect, Qt::KeepAspectRatio);
}
