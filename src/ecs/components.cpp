/*!
 * \file components.cpp
 * \brief Data structure for the entities
 * \author Laurent.D
 * \version 1.0
 * \date 1 décembre 2019
 */

#include "gl-project-2/ecs/components.hpp"

/*!
============================================================================
===============================Part Direction===============================
============================================================================
*/
bool Direction::differentAxe() {
    switch (actualDirection) {
    case TypeDirection::Up:
        return (futurDirection == TypeDirection::Left || futurDirection == TypeDirection::Right);
    case TypeDirection::Down:
        return (futurDirection == TypeDirection::Left || futurDirection == TypeDirection::Right);
    case TypeDirection::Left:
        return (futurDirection == TypeDirection::Up || futurDirection == TypeDirection::Down);
    case TypeDirection::Right:
        return (futurDirection == TypeDirection::Up || futurDirection == TypeDirection::Down);
    }
    return false;
}

/*!
============================================================================
===============================Part CollisionBoxes==========================
============================================================================
*/

CollisionBoxes::CollisionBoxes(QVector<QRectF> boxes, QString layer) : boxes(std::move(boxes)), layer(std::move(layer)) {
    globalSize = totalSize();
}

CollisionBoxes::CollisionBoxes(QRectF box, QString layer) : boxes({std::move(box)}), layer(std::move(layer)) {
    globalSize = totalSize();
}

void CollisionBoxes::addMask(const QVector<QString>& newLayers) {
    for (auto lay : newLayers) {
        mask.insert(lay);
    }
}

void CollisionBoxes::replaceMask(const QVector<QString>& newLayers) {
    mask.clear();
    for (auto lay : newLayers) {
        mask.insert(lay);
    }
}

QSizeF CollisionBoxes::plusScaleBox(const QSizeF& size, const double& plus) {
    QSizeF returnSize{size};
    returnSize.setHeight(returnSize.height() + plus);
    returnSize.setWidth(returnSize.height() + plus);
    return returnSize;
}

QSizeF CollisionBoxes::totalSize() {
    QSizeF size{0, 0};
    for (auto box : boxes) {
        if (size.width() < box.width()) {
            size.setWidth(box.width());
        }
        if (size.height() < box.height()) {
            size.setHeight(box.height());
        }
    }
    return size;
}

Sprite::Sprite(const QVector<Sprite::Frame>& frames, int currentFrame, QSize size)
: frames(frames), currentFrame(currentFrame), size(size), pixmapItem(new QGraphicsPixmapItem) {
    assert(!frames.isEmpty());
    pixmapItem->setPixmap(frames.at(currentFrame).pixmap);
}

Target::Target(const entityx::Entity& entity) : targetKind(Kind::Entity), dataEntity(entity) {}

Target::Target(const QPointF& position) : targetKind(Kind::Position), dataPosition(position) {}

entityx::Entity Target::entity() const {
    assert(targetKind == Target::Kind::Entity);
    return dataEntity;
}

QPointF Target::position() const {
    assert(targetKind == Target::Kind::Position);
    return dataPosition;
}

GhostSoul::GhostSoul(GhostSoul::Color color,
                     GhostSoul::State state,
                     double waitingMsec,
                     const DirectionToFramesMap& normalFrames,
                     const DirectionToFramesMap& fearfulFrames,
                     const DirectionToFramesMap& loserFrames)
: color(color)
, state(state)
, waitingMsec(waitingMsec)
, normalFrames(normalFrames)
, fearfulFrames(fearfulFrames)
, loserFrames(loserFrames)
, remainingMsec(0.0) {}
