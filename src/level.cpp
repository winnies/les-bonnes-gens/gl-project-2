#include "gl-project-2/level.hpp"
#include "gl-project-2/aseprite.hpp"
#include "gl-project-2/ecs/components.hpp"
#include "gl-project-2/ecs/events.hpp"
#include "gl-project-2/ecs/systems/ai.hpp"
#include "gl-project-2/ecs/systems/audio.hpp"
#include "gl-project-2/ecs/systems/gameplay.hpp"
#include "gl-project-2/ecs/systems/input.hpp"
#include "gl-project-2/ecs/systems/physics.hpp"
#include "gl-project-2/ecs/systems/render.hpp"
#include <QDebug>
#include <entityx/deps/Dependencies.h>
#include <tiled/layer.h>
#include <tiled/mapobject.h>
#include <tiled/objectgroup.h>
#include <tiled/tiled.h>
#include <tiled/tilelayer.h>

Level::Level(QWidget* parent) : QWidget(parent), entities(events), systems(entities, events) {
    events.subscribe<GameplayEvent>(*this);

    systems.add<AI>();
    systems.add<Audio>();
    auto gameplay = systems.add<Gameplay>();
    systems.add<Physics>();
    auto input  = systems.add<Input>(this);
    auto render = systems.add<Render>(this);
    systems.add<entityx::deps::Dependency<Role, Exists>>();
    systems.configure();

    score = new QLabel(this);
    score->setStyleSheet("font-family: Times New Roman;"
                         "font-weight: bold;"
                         "font-size: 20px;"
                         "color: white;"
                         "background-color: brown;"
                         "border-bottom: 2px solid green;"
                         "padding: 3px;");
    score->setText("Score : 0");

    bestScore = 0;
    highScore = new QLabel(this);
    highScore->setStyleSheet("font-family: Times New Roman;"
                             "font-weight: bold;"
                             "font-size: 20px;"
                             "color: white;"
                             "background-color: brown;"
                             "border-bottom: 2px solid green;"
                             "padding: 3px;");
    highScore->setText("High score : 0");
    highScore->setAlignment(Qt::AlignCenter);

    life = new QLabel(this);
    life->setStyleSheet("font-family: Times New Roman;"
                        "font-weight: bold;"
                        "font-size: 20px;"
                        "color: white;"
                        "background-color: brown;"
                        "border-bottom: 2px solid green;"
                        "padding: 3px;");
    life->setText("Life : ");
    life->setAlignment(Qt::AlignRight);

    auto* boxLayout = new QHBoxLayout;
    boxLayout->setMargin(0);
    boxLayout->setSpacing(0);
    boxLayout->addWidget(score);
    boxLayout->addWidget(highScore);
    boxLayout->addWidget(life);

    auto* layout = new QGridLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addLayout(boxLayout, 0, 0);
    layout->addWidget(render.get());

    setFocusPolicy(Qt::NoFocus);
    input->setFocus();

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Level::update);

    const char* env = std::getenv("LEVEL");
    loadMap(QStringLiteral("rs:levels/%1.tmx").arg(env != nullptr ? env : QStringLiteral("Autumn")));

    elapsedTime.start();
    timer->start(updateTimeMsec);
}

Level::~Level() {
    systems.system<Input>()->setParent(nullptr);
    systems.system<Render>()->setParent(nullptr);
}

void Level::receive(const GameplayEvent& gameplayEvent) {
    using K = GameplayEvent::Kind;
    switch (gameplayEvent.kind()) {
    case K::GameLost: {
        qDebug() << "perdu";
        initGameplay = true;
        break;
    }
    case K::ScoreChanged: {
        score->setText(QStringLiteral("Score : %1").arg(gameplayEvent.score()));
        if (bestScore < gameplayEvent.score()) {
            bestScore = gameplayEvent.score();
            highScore->setText(QStringLiteral("High score : %1").arg(bestScore));
        }
        break;
    }
    case K::NbLifeRemainingChanged:
        life->setText(QStringLiteral("Life : %1").arg(gameplayEvent.nbLifeRemaining()));
        break;
    default:
        break;
    }
}

void Level::update() {
    entityx::TimeDelta dt = elapsedTime.restart();

    if (initGameplay) {
        initGameplay = false;
        systems.system<Gameplay>()->init();
        systems.system<Gameplay>()->start();
    }

    // process inputs
    systems.update<Input>(dt);

    // update game logic
    systems.update<AI>(dt);
    for (double timeElapsed = dt; timeElapsed > 0.0; timeElapsed -= 16.0) {
        systems.update<Physics>(std::min(16.0, timeElapsed));
    }
    systems.update<Gameplay>(dt);

    // show result
    systems.update<Render>(dt);
    systems.update<Audio>(dt);

    entities.each<DestroyLater>([&](entityx::Entity entity, DestroyLater& /*destroyLater*/) { entity.destroy(); });
}

void Level::loadMap(const QString& fileName) {
    if (!map || (map && map->fileName != fileName)) {
        map = mapReader.readMap(fileName);
    }

    if (map == nullptr) {
        qDebug() << "error while reading " << fileName << ": " << mapReader.errorString();
        return;
    }

    loadMap(*map);
}

void Level::loadMap(const Tiled::Map& map) {
    entities.reset();

    const QSize tileSize = map.tileSize();
    const int tileWidth  = map.tileWidth();
    const int tileHeight = map.tileHeight();

    systems.system<AI>()->tileSize = tileSize;
    systems.system<AI>()->mapSize  = map.size();

    entityx::Entity pacman;
    QPointF ghostOutside;

    Tiled::LayerIterator iterator(&map);
    while (const Tiled::Layer* layer = iterator.next()) {
        switch (layer->layerType()) {
        case Tiled::Layer::TileLayerType: {
            const auto* tileLayer = dynamic_cast<const Tiled::TileLayer*>(layer);

            const auto layerOffset = layer->totalOffset();

            const QPointF layerPos(layerOffset.x() + tileLayer->x() * tileWidth, layerOffset.y() + tileLayer->y() * tileHeight);

            QRect bounds = tileLayer->localBounds();
            int startX   = bounds.left();
            int startY   = bounds.top();
            int endX     = bounds.right();
            int endY     = bounds.bottom();

            if (startX > endX || startY > endY) {
                continue;
            }

            Tiled::Map::RenderOrder renderOrder = map.renderOrder();

            int incX = 1;
            int incY = 1;
            switch (renderOrder) {
            case Tiled::Map::RightUp:
                std::swap(startY, endY);
                incY = -1;
                break;
            case Tiled::Map::LeftDown:
                std::swap(startX, endX);
                incX = -1;
                break;
            case Tiled::Map::LeftUp:
                std::swap(startX, endX);
                std::swap(startY, endY);
                incX = -1;
                incY = -1;
                break;
            case Tiled::Map::RightDown:
                break;
            }

            endX += incX;
            endY += incY;

            for (int y = startY; y != endY; y += incY) {
                for (int x = startX; x != endX; x += incX) {
                    const Tiled::Cell& cell = tileLayer->cellAt(x, y);
                    if (cell.isEmpty()) {
                        continue;
                    }

                    Tiled::Tile* tile = cell.tile();

                    if (tile == nullptr) {
                        continue;
                    }

                    const QPixmap& image = tile->image();
                    QPointF pos(layerPos.x() + x * tileWidth, layerPos.y() + y * tileHeight);

                    entityx::Entity entity = entities.create();
                    entity.assign<Position>(pos);
                    entity.assign<Sprite>(systems.system<Render>()->addSprite({{100, image}}, tileSize));

                    if (tile->type() == "Coin") {
                        entity.assign<Role>(Role::Coin);
                    } else if (tile->type() == "Fruit") {
                        entity.assign<Role>(Role::Fruit);
                    } else if (tile->type() == "PasseMuraille") {
                        entity.assign<Role>(Role::PasseMuraille);
                    } else if (tile->type() == "MapleSyrup") {
                        entity.assign<Role>(Role::MapleSyrup);
                    } else if (tile->type() == "GhostPath") {
                        entity.remove<Sprite>();
                        entity.assign<GhostPath>();
                    } else if (tile->type() == "Bedrock") {
                        entity.remove<Sprite>();
                    }

                    if (tile->objectGroup() == nullptr) {
                        continue;
                    }

                    const QList<Tiled::MapObject*>& objects = tile->objectGroup()->objects();

                    QVector<QRectF> initBoxes;
                    for (const Tiled::MapObject* mObject : objects) {
                        if (mObject == nullptr) {
                            continue;
                        }
                        if (mObject->shape() != Tiled::MapObject::Shape::Rectangle) {
                            continue;
                        }

                        initBoxes.push_back({mObject->position(), mObject->size()});
                    }

                    entity.assign<CollisionBoxes>(initBoxes, tile->type());
                }
            }
        } break;
        case Tiled::Layer::ObjectGroupType: {
            const auto* objectGroup = dynamic_cast<const Tiled::ObjectGroup*>(layer);

            const auto layerOffset = layer->totalOffset();
            const QPointF layerPos(layerOffset.x() + layer->x() * tileWidth, layerOffset.y() + layer->y() * tileHeight);

            const QList<Tiled::MapObject*>& objects = objectGroup->objects();

            for (const Tiled::MapObject* mObject : objects) {
                if (mObject == nullptr) {
                    continue;
                }
                if (mObject->shape() != Tiled::MapObject::Shape::Point) {
                    continue;
                }

                QPointF pos = layerPos + mObject->position();

                if (mObject->type() == "GhostOutside") {
                    ghostOutside = pos;
                    continue;
                }

                pos -= QPoint(tileWidth, tileHeight) / 2;

                entityx::Entity entity = entities.create();
                entity.assign<Position>(pos);

                entity.assign<Spawn>(pos);
                auto collisionBoxes = entity.assign<CollisionBoxes>(QRectF({0.0, 0.0}, tileSize), mObject->type());

                if (mObject->type() == "Pac-Man") {
                    entity.assign<RawSpeed>(0.2F);
                    entity.assign<Role>(Role::PacMan);
                    entity.assign<InputMode>(Qt::Key_Left, Qt::Key_Up, Qt::Key_Right, Qt::Key_Down);
                    collisionBoxes->mask << "Wall"
                                         << "Door"
                                         << "Bedrock";
                    auto dir  = entity.assign<Direction>(Direction::TypeDirection::Down);
                    auto dtfm = entity.assign<DirectionToFramesMap>();
                    dtfm->frames[Direction::TypeDirection::Up] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/PacMan/PacPinkUp.json"));
                    dtfm->frames[Direction::TypeDirection::Down] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/PacMan/PacPinkDown.json"));
                    dtfm->frames[Direction::TypeDirection::Left] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/PacMan/PacPinkLeft.json"));
                    dtfm->frames[Direction::TypeDirection::Right] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/PacMan/PacPinkRight.json"));
                    entity.assign<Sprite>(systems.system<Render>()->addSprite(dtfm->frames[dir->getActualDirection()], tileSize));

                    pacman = entity;
                }

                if (mObject->type() == "Ghost") {
                    collisionBoxes->mask << "Wall"
                                         << "Bedrock";
                    entity.assign<Role>(Role::Ghost);

                    auto dir   = entity.assign<Direction>(Direction::TypeDirection::Up);
                    auto color = mObject->property(QStringLiteral("color")).toString();

                    DirectionToFramesMap normalFrames;
                    normalFrames.frames[Direction::TypeDirection::Up] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainUp%1.json").arg(color));
                    normalFrames.frames[Direction::TypeDirection::Down] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainDown%1.json").arg(color));
                    normalFrames.frames[Direction::TypeDirection::Left] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainLeft%1.json").arg(color));
                    normalFrames.frames[Direction::TypeDirection::Right] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainRight%1.json").arg(color));

                    DirectionToFramesMap fearfulFrames;
                    fearfulFrames.frames[Direction::TypeDirection::Up] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainUpFlee.json"));
                    fearfulFrames.frames[Direction::TypeDirection::Down] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainDownFlee.json"));
                    fearfulFrames.frames[Direction::TypeDirection::Left] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainLeftFlee.json"));
                    fearfulFrames.frames[Direction::TypeDirection::Right] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainRightFlee.json"));

                    DirectionToFramesMap loserFrames;
                    loserFrames.frames[Direction::TypeDirection::Up] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainUpDeath.json"));
                    loserFrames.frames[Direction::TypeDirection::Down] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainDownDeath.json"));
                    loserFrames.frames[Direction::TypeDirection::Left] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainLeftDeath.json"));
                    loserFrames.frames[Direction::TypeDirection::Right] =
                        readAsepriteFrames(QStringLiteral("rs:sprites/Villain/VillainRightDeath.json"));

                    GhostSoul::Color colorGhost;

                    if (color == "Red") {
                        colorGhost = GhostSoul::Color::Red;
                        entity.assign<RawSpeed>(0.15F);
                    } else if (color == "Green") {
                        colorGhost = GhostSoul::Color::Green;
                        entity.assign<RawSpeed>(0.3F);
                    } else if (color == "Blue") {
                        colorGhost = GhostSoul::Color::Blue;
                        entity.assign<RawSpeed>(0.17F);
                    } else if (color == "Yellow") {
                        colorGhost = GhostSoul::Color::Yellow;
                        entity.assign<RawSpeed>(0.2F);
                    } else {
                        assert(false);
                    }

                    double waitingMsec = 3000;

                    entity.assign<GhostSoul>(
                        colorGhost, GhostSoul::State::Waiting, waitingMsec, normalFrames, fearfulFrames, loserFrames);
                    entity.assign<DirectionToFramesMap>(normalFrames);
                    entity.assign<Sprite>(
                        systems.system<Render>()->addSprite(normalFrames.frames[dir->getActualDirection()], tileSize));
                }
            }
        } break;
        default:
            continue;
        }
    }

    entities.each<GhostSoul>([&](entityx::Entity /*entity*/, GhostSoul& ghostSoul) {
        ghostSoul.pacman  = pacman;
        ghostSoul.outside = ghostOutside;
    });
}
