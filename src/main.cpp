#include "gl-project-2/application.hpp"

int main(int argc, char* argv[]) {
    Application app(argc, argv);
    return Application::exec();
}
