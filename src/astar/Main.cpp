#include "gl-project-2/astar/astar.hpp"
#include "gl-project-2/astar/grid.hpp"
#include "gl-project-2/astar/node.hpp"
#include <QPoint>
#include <QVector>
#include <iostream>

using namespace std;

int main() {
    QVector<bool> boules = {1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1,
                            0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0};

    // Grid grille(boules, 7);

    // cout << "La grille fait " << grille.lignNumber << " lignes et " << grille.columnNumber << " colonnes" << endl;

    // grille.displayGrid();

    Astar algo(boules, 7);
    QPoint hunter(0, 0);
    QPoint target(1, 6);

    if (algo.grid.get(hunter).walkable == true && algo.grid.get(target).walkable == true)
        cout << "Start node and target node walkable, proceed." << endl;
    else {
        cout << "Start node and/or target node not walkable, error" << endl;
        exit(1);
    }

    QVector<QPoint> path = algo.findPath(hunter, target);

    cout << "Pour hunter etant en [" << hunter.x() << " : " << hunter.y() << "] et target en [" << target.x() << " : "
         << target.y() << "] chemin de taille " << path.size() << " trouve : " << endl;
    for (int i = 0; i < path.size(); i++) {
        cout << "(" << path.at(i).x() << " : " << path.at(i).y() << ") - ";
    }
    cout << endl;
}
