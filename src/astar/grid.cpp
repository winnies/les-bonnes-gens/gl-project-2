#include "gl-project-2/astar/grid.hpp"
#include <iostream>

using namespace std;

Grid::Grid() : columnNumber(-1), lignNumber(-1) {}

Grid::Grid(const QVector<bool>& boules, int columnNumber) : columnNumber(columnNumber), lignNumber(boules.size() / columnNumber) {
    for (int i = 0; i < lignNumber; i++) {
        for (int j = 0; j < columnNumber; j++) {
            grid.push_back(Node({i, j}, boules.at(i * columnNumber + j)));
            // cout << get({i,j}).walkable << " ";
        }
        // cout << endl;
    }
}

/*
vector<Node> Grid::getNeighbours(Node node)
{
  vector<Node> neighbours;

  for (int x=-1; x<1; x++)
  {
    for (int y=-1; y<1; y++)
    {
      if(x == 0 && y == 0)
        continue;

      int checkX = node.gridX +x;
      int checkY = node.gridY +y;

      if(checkX >=0 && checkX < columnNumber && checkY >=0 && checkY < lignNumber)
        neighbours.push_back(get(checkX, checkY));
    }
  }

  return neighbours;
} */

QVector<Node> Grid::getNeighbours(const Node& node) {
    // cout << endl << "------------------------" << endl;
    // cout << "checking neighbours of node at " << node.gridX << "," << node.gridY << endl;
    QVector<Node> neighbours;

    for (int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
            // cout << "x = " << x << " y = " << y << endl;
            if ((x == 0 && y == 0) || (x == -1 && y == -1) || (x == -1 && y == 1) || (x == 1 && y == -1) || (x == 1 && y == 1)) {
                // cout << "neighbour " << x << " : " << y << " ingored" << endl;
                continue;
            }

            // int checkX = node.coord.x() +x;
            // int checkY = node.coord.y() +y;
            QPoint check(node.coord.x() + x, node.coord.y() + y);

            if ((check.x() >= 0 && check.x() < lignNumber) && (check.y() >= 0 && check.y() < columnNumber)) {
                // cout << "neigbour " << checkX << " : " << checkY << " added" << endl;
                neighbours.push_back(get(check));
            } else {
                // cout << "neighbour " << checkX << " : " << checkY << " unaccepted" << endl;
            }
        }
    }

    // cout << endl << neighbours.size() << " neighbours found for Node at " << node.gridX << "," << node.gridY << endl;
    // cout << "------------------------" << endl << endl;

    return neighbours;
}

void Grid::displayGrid() {
    for (int i = 0; i < lignNumber; i++) {
        for (int j = 0; j < columnNumber; j++) {
            cout << get({i, j}).walkable << " ";
        }
        cout << endl;
    }
}
