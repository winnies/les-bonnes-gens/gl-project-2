#include "gl-project-2/astar/node.hpp"
#include <iostream>

Node::Node() : gCost(0), hCost(0) {}

Node::Node(const QPoint& coord, bool walkable) : walkable(walkable), gCost(0), hCost(0), coord(coord) {}
