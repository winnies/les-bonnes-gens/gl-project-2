#include "gl-project-2/astar/astar.hpp"
#include "gl-project-2/astar/node.hpp"
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

Astar::Astar(const QVector<bool>& boules, int columnNumber) : grid(boules, columnNumber){};

Astar::Astar(const Grid& grid) : grid(grid){};

int getDistance(const Node& nodeA, const Node& nodeB) {
    // cout << "getDistance between node " << nodeA.coord.x() << "," << nodeA.coord.y() << " and " << nodeB.coord.x() << "," <<
    // nodeB.coord.y() << endl;
    int xCost = std::abs(nodeA.coord.x() - nodeB.coord.x());
    int yCost = std::abs(nodeA.coord.y() - nodeB.coord.y());
    // cout << "getDistance = " << xCost + yCost << endl;
    return xCost + yCost;
}

QVector<QPoint> Astar::findPath(const QPoint& startPos, const QPoint& targetPos) {
    // cout << "[entering algo, looking for a path]" << endl;
    Node startNode  = grid.get(startPos);
    Node targetNode = grid.get(targetPos);

    QVector<Node> openSet;
    QVector<Node> closedSet;

    openSet.push_back(startNode);
    // openSet.push_back(targetNode);

    // if(startPos[0] == targetPos[0] && startPos[1] == targetPos[1])
    //     // envoi un event

    while (!openSet.empty()) {
        // cout << endl << "Open set size = " << openSet.size() << endl;
        // cout << "Openset : " << endl;
        // for(int i=0; i<openSet.size(); i++)
        // {
        //     cout << openSet.at(i).coord.x() << openSet.at(i).coord.y() << " : g = " << openSet.at(i).gCost << " h = " <<
        //     openSet.at(i).hCost << endl;
        // }

        Node currentNode = openSet.front();
        // cout << endl << "first Node of Openset : " << openSet.front().coord.x() << " / " << openSet.front().coord.y() << endl;

        // cout << "comparing which node has the best f cost of the openSet" << endl;
        for (int i = 1; i < openSet.size(); i++) {
            // cout << "fcost : " << openSet.at(i).fCost() << " ; " << currentNode.fCost() << " // hcost openSet(i)/currentNode :
            // " << openSet.at(i).hCost << " ; " << currentNode.hCost << endl;
            if ((openSet.at(i).fCost() < currentNode.fCost()) ||
                (openSet.at(i).fCost() == currentNode.fCost() && openSet.at(i).hCost < currentNode.hCost)) {
                currentNode = openSet.at(i);
                // cout << "Node at " << currentNode.coord.x() << "," << currentNode.gridY << " choisis" << endl;
            }
        }
        // cout << "current Node (after comparing fCosts) = " << currentNode.coord.x() << " / " << currentNode.coord.y() << endl;
        openSet.removeOne(currentNode);
        closedSet.push_back(currentNode);

        if (currentNode.coord == targetNode.coord) {
            // cout << "current Node = " << currentNode.coord.x() << "," << currentNode.coord.y() << " son parent est " <<
            // currentNode.parentCoord.x() << "," << currentNode.parentCoord.y() << endl; cout << endl << "[path found, exiting
            // algo]" << endl;
            // cout << "start Node = " << startNode.coord.x() << "," << startNode.parentCoord.y() << endl;
            // while(currentNode.parentCoord.x() != startNode.coord.x() || currentNode.parentCoord.y() != startNode.coord.y())
            // {
            //   cout << "-> " << currentNode.coord.x() << "," << currentNode.coord.y() << " parent = " <<
            //   currentNode.parentCoord.x() << "," << currentNode.parentCoord.y() << endl; currentNode =
            //   grid.get(currentNode.parentCoord.x(), currentNode.parentCoord.y()); cout << "donc le parent est bien = " <<
            //   currentNode.coord.x() << "," << currentNode.coord.y()  << endl;
            // }
            return retracePath(startNode, currentNode);
        }

        QVector<Node> neighbours = grid.getNeighbours(currentNode);
        for (auto& neighbour : neighbours) {
            // cout << "---->Testing neighbour at " << neighbour.coord.x() << "," << neighbour.coord.y() << endl;
            if (!neighbour.walkable || closedSet.contains(neighbour)) {
                // cout << "Neighbour not walkable or in closedSet" << endl;
                continue;
            }

            int newMovementCostToNeighbour = currentNode.gCost + getDistance(currentNode, neighbour);

            if (newMovementCostToNeighbour < neighbour.gCost || !openSet.contains(neighbour)) {
                neighbour.gCost = newMovementCostToNeighbour;
                neighbour.hCost = getDistance(neighbour, targetNode);
                // cout << "Neighbour at " << neighbour.coord.x() << "," << neighbour.coord.y() << " gcost = " <<
                // neighbour.gCost << " hcost = " << neighbour.hCost << endl; cout << "parent: " <<
                // (neighbour.parent == nullptr) << endl;
                // cout << " --------------> " << neighbour.parent_hCost << " = " << currentNode.hCost << " / " <<
                // neighbour.parent->gCost << " = " << currentNode.gCost << endl;

                neighbour.parentCoord = currentNode.coord;
                // cout << "--------------------------------" << endl;
                // cout << "Ajout du node " << currentNode.coord.x() << "," << currentNode.coord.y() << " comme parent du node "
                // << neighbour.coord.x() << "," << neighbour.coord.y() << endl; cout <<
                // "--------------------------------" << endl;

                if (!openSet.contains(neighbour)) {
                    // cout << "adding neighbour " << neighbour.coord.x() << "," << neighbour.coord.y() << " to the
                    // openSet" << endl;
                    openSet.push_back(neighbour);
                    // grid.setParent(neighbour.coord.x(), neighbour.coord.y(), neighbour.parentCoord.x(),
                    // neighbour.parentCoord.y());
                    grid.get(neighbour.coord).parentCoord = neighbour.parentCoord;
                } else {
                    // cout << "Neighbour already present in the openset" << endl;
                }
            }
        }
    }
    QVector<QPoint> emptyVec;
    // cout << "Aucun chemin trouvé open set vide, renvoi d'un vecteur de node vide" << endl;
    return emptyVec;
}

QVector<QPoint> Astar::retracePath(const Node& startNode, const Node& endNode) {
    QVector<QPoint> path;
    Node currentNode = endNode;
    // cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAH -> " << grid.get(4,1).parentCoord.x() << "," << grid.get(4,1).parentCoord.y() << endl;
    // cout << "current Node : " << currentNode.coord.x() << "," << currentNode.coord.y() <<" start node : " <<
    // startNode.coord.x() << "," << startNode.coord.y() << endl; cout << "Parent du current Node : " <<
    // currentNode.parentCoord.x() << "," << currentNode.parentCoord.y() <<" parent du start node : " << startNode.parentCoord.x()
    // << "," << startNode.parentCoord.y() << endl;

    while (currentNode.coord != startNode.coord) {
        // cout << "RETRACING PATH -----------> node at " << currentNode.coord.x() << "," << currentNode.coord.y() << " added" <<
        // endl; cout << "RETRACING PATH -----------> Le parent du node at " << currentNode.coord.x() << "," <<
        // currentNode.coord.y() << " est : " << currentNode.parentCoord.x() << "," << currentNode.parentCoord.y() << endl;
        path.push_back(QPoint(currentNode.coord));
        currentNode = grid.get(currentNode.parentCoord);
        // cout << "RETRACING PATH -----------> new node = " << currentNode.coord.x() << "," << currentNode.coord.y() << endl;
        // currentNode.coord.x() = currentNode.parentCoord.x();
        // currentNode.coord.y() = currentNode.parentCoord.y();
    }
    path.push_back(QPoint(startNode.coord));
    return path;
}
