#!/bin/bash
source scripts/deps.sh

export ECM_ROOT=${DEPS}/ecm

if [ ! -d ${ECM_ROOT} ]; then
    WORK_DIR=$(mktemp -d -t ecm-git.XXX)
    git clone --branch v5.63.0 https://github.com/KDE/extra-cmake-modules.git ${WORK_DIR}
    cmake -DCMAKE_INSTALL_PREFIX=${ECM_ROOT} \
          -DBUILD_HTML_DOCS=OFF \
          -DBUILD_QTHELP_DOCS=ON \
          -DBUILD_TESTING=OFF \
          -S ${WORK_DIR} -B ${WORK_DIR}/build
    cmake --build ${WORK_DIR}/build --parallel $(nproc)
    cmake --install ${WORK_DIR}/build
    rm -rf ${WORK_DIR}
fi
