#!/bin/bash
set -ex

lsb_release -a
g++ --version

# config deps
source /scripts/setup_ci.sh

# build project
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -S . -B build
cmake --build build --target check-format
cmake --build build --target clang-tidy-parallel
cmake --build build -v

# deploy
cd build
DESTDIR=AppDir make install
wget -c -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
chmod a+x linuxdeployqt-continuous-x86_64.AppImage
cd ..
export VERSION=${CI_COMMIT_REF_NAME}
./build/linuxdeployqt-continuous-x86_64.AppImage --appimage-extract-and-run build/AppDir/usr/share/applications/*.desktop -appimage
