#!/bin/bash

export DEPS=/opt
export ONLY_LIBTILED=true

source /opt/qt512/bin/qt512-env.sh || true
source scripts/install_cmake.sh
source scripts/install_ecm.sh
source scripts/install_entityx.sh
source scripts/install_tiled.sh
