#!/bin/bash
source scripts/deps.sh

export TILED_ROOT=${DEPS}/tiled

if [ ! -d ${TILED_ROOT} ]; then
    WORK_DIR=$(mktemp -d -t tiled-git.XXX)
    git clone --branch v1.3.0 https://github.com/bjorn/tiled.git ${WORK_DIR}
    mkdir -p ${WORK_DIR}/build && cd ${WORK_DIR}/build
    qmake -r INSTALL_HEADERS=yes RPATH=no PREFIX=/ ${WORK_DIR}
    cd -
    
    MAKE_DIR=${WORK_DIR}/build
    if [ -z ${ONLY_LIBTILED+x} ]; then
        echo "compile all tiled";
    else
        echo "compile only libtiled";
        MAKE_DIR=${MAKE_DIR}/src/libtiled
    fi

    make -C ${MAKE_DIR} -j$(nproc)
    make -C ${MAKE_DIR} install INSTALL_ROOT=${TILED_ROOT}
    
    rm -rf ${WORK_DIR}
fi

export PATH=${TILED_ROOT}/bin:${PATH}
export LD_LIBRARY_PATH=${TILED_ROOT}/lib:${LD_LIBRARY_PATH}
