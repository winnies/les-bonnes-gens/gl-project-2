#!/bin/bash

export PATH=/opt/Qt/5.13.0/gcc_64/bin:$PATH

source scripts/install_cmake.sh
source scripts/install_ecm.sh
source scripts/install_entityx.sh
source scripts/install_tiled.sh

cmake -S . -B build
cmake --build build --parallel $(nproc)

echo 'executable at build/gl-project-2'
