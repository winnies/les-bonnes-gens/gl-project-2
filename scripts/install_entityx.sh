#!/bin/bash
source scripts/deps.sh

export ENTITYX_ROOT=${DEPS}/entityx

if [ ! -d ${ENTITYX_ROOT} ]; then
    WORK_DIR=$(mktemp -d -t entityx-git.XXX)
    git clone --branch 1.3.0 https://github.com/alecthomas/entityx.git ${WORK_DIR}
    cmake -DCMAKE_INSTALL_PREFIX=${ENTITYX_ROOT} \
          -DCMAKE_BUILD_TYPE=Release \
          -DENTITYX_BUILD_SHARED=1 \
          -DENTITYX_BUILD_TESTING=0 \
          -Wno-dev \
          -S ${WORK_DIR} -B ${WORK_DIR}/build
    cmake --build ${WORK_DIR}/build --parallel $(nproc)
    cmake --install ${WORK_DIR}/build
    rm -rf ${WORK_DIR}
fi

export LD_LIBRARY_PATH=${ENTITYX_ROOT}/lib:${LD_LIBRARY_PATH}
