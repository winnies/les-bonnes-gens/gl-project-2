#!/bin/bash
source scripts/deps.sh

if [ ! -d ${DEPS}/cmake ]; then
    CMAKE_URL="https://github.com/Kitware/CMake/releases/download/v3.15.4/cmake-3.15.4-Linux-x86_64.tar.gz"
    mkdir ${DEPS}/cmake
    wget --quiet -O - ${CMAKE_URL} | tar --strip-components=1 -xz -C ${DEPS}/cmake
fi

export PATH=${DEPS}/cmake/bin:${PATH}
